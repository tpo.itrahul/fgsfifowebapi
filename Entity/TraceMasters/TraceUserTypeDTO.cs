﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TraceMasters
{
 public class TraceUserTypeDTO
    {
        public int User_Type_Id { get; set; }

        public string? User_Type_Name { get; set; }

        public int? User_Type_Status { get; set; }
    }
}
