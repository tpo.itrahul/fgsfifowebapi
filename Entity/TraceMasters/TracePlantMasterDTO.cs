﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TraceMasters
{
   public class TracePlantMasterDTO
    {
       
        public int Plant_Id { get; set; }

        public string? Plant_Name { get; set; }
        public string? Plant_Address { get; set; }
        public int? Plant_Status { get; set; }
    }
}
