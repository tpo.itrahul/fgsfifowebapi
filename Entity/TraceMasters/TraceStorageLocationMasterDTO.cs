﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TraceMasters
{
  public class TraceStorageLocationMasterDTO
    {


        public int Storage_Loc_Id { get; set; }

        public string? Storage_Loc_Name { get; set; }
        public int? Storage_Loc_Status { get; set; }
        public int? WorkCent_Id { get; set; }

        public string? Type { get; set; }
    }
}
