﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TreadFIFO
{
  public  class TraceHoldReasonsDTO
    {
     
        public int Hold_Reason_Id { get; set; }

       
        public string Hold_Reason { get; set; }

        public int? Hold_Reason_status { get; set; }
    }
}
