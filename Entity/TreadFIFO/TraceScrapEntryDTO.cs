﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TreadFIFO
{
   public class TraceScrapEntryDTO
    {

        public int Scrap_EntryId { get; set; }
       
        public int Material_Id { get; set; }
       
        public string? Updated_By { get; set; }
        public DateTime? Entry_Date { get; set; }
        public DateTime? Entry_Time { get; set; }

    }
}
