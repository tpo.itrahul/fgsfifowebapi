﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TreadFIFO
{
   public class TraceTakenToTbDTO
    {
        
        public int TakenToTb_Id { get; set; }
    
        public int? Production_Id { get; set; }
        public int? RequestID { get; set; }
        public DateTime? TakenToTb_IssuedTime { get; set; }
        public DateTime? TakenToTB_ReqTime { get; set; }
       
        public string? TakenToTB_By { get; set; }
        public int? Status { get; set; }
        public int? Issue_Status { get; set; }
        public int? Mhe_Id { get; set; }
     


    }
}
