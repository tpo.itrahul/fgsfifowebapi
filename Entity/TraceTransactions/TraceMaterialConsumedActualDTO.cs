﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TraceTransactions
{
    public class TraceMaterialConsumedActualDTO
    {
        
        public int MC_Id { get; set; }
       
        public int Production_Id { get; set; }
       
        public int WorkCent_Id { get; set; }
        public DateTime? Production_Date { get; set; }
       
        public string? Production_Shift { get; set; }

        public int? Schedule_ID { get; set; }
        public int? Status { get; set; }

    }
}
