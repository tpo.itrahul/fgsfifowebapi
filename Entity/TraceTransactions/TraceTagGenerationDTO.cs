﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;

namespace Entity.TraceTransactions
{
   public class TraceTagGenerationDTO
    {
        public int TagId { get; set; }
        public string TagDetails { get; set; }
        public int Location_Id { get; set; }
        public string Printer_Name { get; set; }
        public string Printer_Ip { get; set; }
        public DateTime Printed_DateTime { get; set; }
        public string? Module_Name { get; set; }
        public int? User_Id { get; set; }
        public int Print_Status { get; set; }

        public int CreatedModuleRowId { get; set; }
           public int? InvTranId { get; set; }
        //  public FGS_FIFO_Daily_Receipt_Transaction FGS_FIFO_Daily_Receipt_Transaction { get; set; }
    }
}
