﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TraceTransactions
{
    public class TraceMaterialConsumedSpecDTO
    {
       
        public int MCS_Id { get; set; }
   
        public int Production_Id { get; set; }
        public int? Incoming_Material_Id { get; set; }
       
        public string? Incoming_Material_Type { get; set; }
        public int status { get; set; }
    }
}
