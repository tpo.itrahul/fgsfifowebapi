﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;

namespace Entity.FGSFIFO
{
 public  class BatchTypeMasterDTO
    {
       
        public int Batch_Type_Id { get; set; }
        public string Batch_Type_Name { get; set; }

        public int? Batch_Type_Status { get; set; }
    }
}
