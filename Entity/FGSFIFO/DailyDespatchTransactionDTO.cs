﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;


namespace Entity.FGSFIFO
{
  public  class DailyDespatchTransactionDTO
    {
       
        public int Desp_TransId { get; set; }

        public int Desp_Req_Id { get; set; }
        public FGS_FIFO_Despatch_Request FGS_FIFO_Despatch_Request { get; set; }
        public int? Inv_TransId { get; set; }
        public int Quantity_Despatched { get; set; }
        public string? EntryDate { get; set; }
        public string? Entered_By { get; set; }

        public int? Desp_Status_Id { get; set; }
        public FGS_FIFO_Despatch_Status_Master FGS_FIFO_Despatch_Status_Master { get; set; }
        public string vehicle_number { get; set; }
        public string Location { get; set; }
        public string MaterialDesc { get; set; }
        public int MaterialId { get; set; }

        public int TotDespatched { get; set; }

      public string Barcode { get; set; }



    }
}
