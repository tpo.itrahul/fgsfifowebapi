﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;

namespace Entity.FGSFIFO
{
  public  class MaterialMasterDTO
    {
        public int Material_Id { get; set; }
        public string Material_Code { get; set; }
        public string Material_Description { get; set; }
        public int Material_Type_Id { get; set; }
        public string EntryDate { get; set; }
        public int Material_Status { get; set; }
        public FGS_FIFO_Material_Type_Master FGS_FIFO_Material_Type_Master { get; set; }
        public string Material_Type_Name { get; set; }
        public string Material { get; set; }

        public DateTime ProdDate { get; set; }
        public string MatCodeProdDate { get; set; }

    }
}
