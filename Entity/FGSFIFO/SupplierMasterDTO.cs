﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.FGSFIFO
{
  public class SupplierMasterDTO
    {
       
        public int Supplier_Id { get; set; }

       
        public string Supplier_Code { get; set; }

        
        public string Supplier_Name { get; set; }

       
        public string Supplier_Contact_No { get; set; }

        public string EntryDate { get; set; }

        public int Supplier_Status { get; set; }
    }
}
