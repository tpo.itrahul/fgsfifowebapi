﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;

namespace Entity.FGSFIFO
{
 public   class LocationMasterDTO
    {
        public int Location_Id { get; set; }
        public string Location_Name { get; set; }
        public int? Location_Is_Empty { get; set; }
        public int? Location_Status { get; set; }
    }
}
