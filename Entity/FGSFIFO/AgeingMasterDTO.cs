﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;

namespace Entity.FGSFIFO
{
   public class AgeingMasterDTO
    {
      
        public int Ageing_Id { get; set; }
        public int? Material_Type_Id { get; set; }
              
        public int Material_Id { get; set; }
        public FGS_FIFO_Material_Master FGS_FIFO_Material_Master { get; set; }
        public int? Ageing_Months { get; set; }

        public int? Status { get; set; }
        public string Material_Desc { get; set; }
    }
}
