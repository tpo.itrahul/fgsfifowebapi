﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;

namespace Entity.FGSFIFO
{
   public class CustomerMasterDTO
    {
        public int Customer_Id { get; set; }

        public string Customer_Name { get; set; }

        public string Customer_Address { get; set; }


        public int? Batch_Type_Id { get; set; }
      
        public int Customer_Type_Id { get; set; }
        public FGS_FIFO_Customer_Type_Master FGS_FIFO_Customer_Type_Master { get; set; }
        public string Customer_Type_Name { get; set; }
        public int? Cust_Status { get; set; }
        public int CustAgeinId { get; set; }
        public string BatchTypeName { get; set; }
        public ICollection<FGS_FIFO_Cust_Ageing_Mapping> FGS_FIFO_Cust_Ageing_Mapping { get; set; }
        public FGS_FIFO_Batch_Type_Master FKFGS_FIFO_Batch_Type_Master { get; set; }

    }
}
