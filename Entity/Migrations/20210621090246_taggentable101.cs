﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entity.Migrations
{
    public partial class taggentable101 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FGS_FIFO_Ageing_Master",
                columns: table => new
                {
                    Ageing_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Ageing_Months = table.Column<int>(type: "int", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FGS_FIFO_Ageing_Master", x => x.Ageing_Id);
                });

            migrationBuilder.CreateTable(
                name: "FGS_FIFO_Batch_Type_Master",
                columns: table => new
                {
                    Batch_Type_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Batch_Type_Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Batch_Type_Status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FGS_FIFO_Batch_Type_Master", x => x.Batch_Type_Id);
                });

            migrationBuilder.CreateTable(
                name: "FGS_FIFO_Customer_Type_Master",
                columns: table => new
                {
                    Customer_Type_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Customer_Type_Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Customer_Type_Status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FGS_FIFO_Customer_Type_Master", x => x.Customer_Type_Id);
                });

            migrationBuilder.CreateTable(
                name: "FGS_FIFO_Despatch_Status_Master",
                columns: table => new
                {
                    Desp_Status_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Desp_Status_Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FGS_FIFO_Despatch_Status_Master", x => x.Desp_Status_Id);
                });

            migrationBuilder.CreateTable(
                name: "FGS_FIFO_Location_Master",
                columns: table => new
                {
                    Location_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Location_Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Location_Is_Empty = table.Column<int>(type: "int", nullable: false),
                    Location_Status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FGS_FIFO_Location_Master", x => x.Location_Id);
                });

            migrationBuilder.CreateTable(
                name: "FGS_FIFO_Material_Type_Master",
                columns: table => new
                {
                    Material_Type_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Material_Type_Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Material_Type_Status = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FGS_FIFO_Material_Type_Master", x => x.Material_Type_Id);
                });

            migrationBuilder.CreateTable(
                name: "FGS_FIFO_Supplier_Master",
                columns: table => new
                {
                    Supplier_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Supplier_Code = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Supplier_Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Supplier_Contact_No = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    EntryDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Supplier_Status = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FGS_FIFO_Supplier_Master", x => x.Supplier_Id);
                });

            migrationBuilder.CreateTable(
                name: "Trace_Application_Master",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Application_Id = table.Column<int>(type: "int", nullable: false),
                    Application_Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Application_Status = table.Column<int>(type: "int", nullable: false),
                    Plant_Id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trace_Application_Master", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Trace_Barcode_Generation",
                columns: table => new
                {
                    Barcode_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Barcode_Gen_Date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Barcode_Gen_Time = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Production_Id = table.Column<int>(type: "int", nullable: false),
                    Barcode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    BarcodeGen_Status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trace_Barcode_Generation", x => x.Barcode_Id);
                });

            migrationBuilder.CreateTable(
                name: "Trace_Hold",
                columns: table => new
                {
                    Hold_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Production_ID = table.Column<int>(type: "int", nullable: false),
                    Hold_Reason_Id = table.Column<int>(type: "int", nullable: false),
                    Hold_By = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Hold_Time = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Hold_Date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Hold_Status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trace_Hold", x => x.Hold_Id);
                });

            migrationBuilder.CreateTable(
                name: "Trace_Hold_Reasons",
                columns: table => new
                {
                    Hold_Reason_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Hold_Reason = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Hold_Reason_status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trace_Hold_Reasons", x => x.Hold_Reason_Id);
                });

            migrationBuilder.CreateTable(
                name: "Trace_Hold_Release",
                columns: table => new
                {
                    Hold_Release_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Hold_Id = table.Column<int>(type: "int", nullable: false),
                    Realesed_By = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Realesed_Count = table.Column<int>(type: "int", nullable: false),
                    Released_Time = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Realesed_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Released_Reason = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    status = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trace_Hold_Release", x => x.Hold_Release_Id);
                });

            migrationBuilder.CreateTable(
                name: "Trace_Login_Details",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Emp_Id = table.Column<int>(type: "int", nullable: false),
                    Module_Id = table.Column<int>(type: "int", nullable: false),
                    Logged_Time = table.Column<int>(type: "int", nullable: false),
                    Plant_Id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trace_Login_Details", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Trace_Login_Master",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Emp_Id = table.Column<int>(type: "int", nullable: false),
                    User_Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Plant_Id = table.Column<int>(type: "int", nullable: false),
                    UserRole = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trace_Login_Master", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Trace_Material_Consumed_Actual",
                columns: table => new
                {
                    MC_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Production_Id = table.Column<int>(type: "int", nullable: false),
                    WorkCent_Id = table.Column<int>(type: "int", nullable: false),
                    Production_Date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Production_Shift = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true),
                    Schedule_ID = table.Column<int>(type: "int", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trace_Material_Consumed_Actual", x => x.MC_Id);
                });

            migrationBuilder.CreateTable(
                name: "Trace_Material_Consumed_Spec",
                columns: table => new
                {
                    MCS_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Production_Id = table.Column<int>(type: "int", nullable: false),
                    Incoming_Material_Id = table.Column<int>(type: "int", nullable: true),
                    Incoming_Material_Type = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    status = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trace_Material_Consumed_Spec", x => x.MCS_Id);
                });

            migrationBuilder.CreateTable(
                name: "Trace_Material_Master",
                columns: table => new
                {
                    Material_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Material_Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    WorkCent_Id = table.Column<int>(type: "int", nullable: false),
                    Material_Type = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Material_CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Material_CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trace_Material_Master", x => x.Material_Id);
                });

            migrationBuilder.CreateTable(
                name: "Trace_Material_Transaction",
                columns: table => new
                {
                    Material_Transaction_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Production_Id = table.Column<int>(type: "int", nullable: false),
                    Material_Status = table.Column<int>(type: "int", nullable: true),
                    Start_Time = table.Column<DateTime>(type: "datetime2", nullable: true),
                    End_Time = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Created_By = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Created_Time = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Trans_Status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trace_Material_Transaction", x => x.Material_Transaction_Id);
                });

            migrationBuilder.CreateTable(
                name: "Trace_MHE_Master",
                columns: table => new
                {
                    MHE_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    WorkCent_Id = table.Column<int>(type: "int", nullable: false),
                    MHE_Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MHE_Type = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MHE_Status = table.Column<int>(type: "int", nullable: true),
                    Max_Capacity = table.Column<int>(type: "int", nullable: true),
                    Unit_Of_Measurement = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trace_MHE_Master", x => x.MHE_Id);
                });

            migrationBuilder.CreateTable(
                name: "Trace_Module_Master",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Module_Id = table.Column<int>(type: "int", nullable: false),
                    Module_Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Application_Id = table.Column<int>(type: "int", nullable: false),
                    Module_Status = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trace_Module_Master", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Trace_Plant_Master",
                columns: table => new
                {
                    Plant_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Plant_Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Plant_Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Plant_Status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trace_Plant_Master", x => x.Plant_Id);
                });

            migrationBuilder.CreateTable(
                name: "Trace_Print_Generation",
                columns: table => new
                {
                    Print_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Production_Id = table.Column<int>(type: "int", nullable: false),
                    Print_Time = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Printed_By = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Barcode_Id = table.Column<int>(type: "int", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trace_Print_Generation", x => x.Print_Id);
                });

            migrationBuilder.CreateTable(
                name: "Trace_Prod_Remarks",
                columns: table => new
                {
                    Remark_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Remarks = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    WorkCent_Id = table.Column<int>(type: "int", nullable: false),
                    Remark_Status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trace_Prod_Remarks", x => x.Remark_Id);
                });

            migrationBuilder.CreateTable(
                name: "Trace_Product_Ageing",
                columns: table => new
                {
                    Product_Ageing_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Material_ID = table.Column<int>(type: "int", nullable: false),
                    Ageing_Time = table.Column<int>(type: "int", nullable: true),
                    OverAgeing_Time = table.Column<int>(type: "int", nullable: true),
                    Ageing_Unit = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trace_Product_Ageing", x => x.Product_Ageing_Id);
                });

            migrationBuilder.CreateTable(
                name: "Trace_Production",
                columns: table => new
                {
                    Production_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Material_Id = table.Column<int>(type: "int", nullable: false),
                    WorkCent_Id = table.Column<int>(type: "int", nullable: false),
                    Prod_Date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Prod_Time = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CrewDetails = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    AdditionalFields = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    Remarks = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    Quantity = table.Column<int>(type: "int", nullable: true),
                    Unit_Of_Measurement = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MHE_Id = table.Column<int>(type: "int", nullable: false),
                    Barcode_Id = table.Column<int>(type: "int", nullable: true),
                    Shift_Id = table.Column<int>(type: "int", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trace_Production", x => x.Production_Id);
                });

            migrationBuilder.CreateTable(
                name: "Trace_Scrap_Entry",
                columns: table => new
                {
                    Scrap_EntryId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Material_Id = table.Column<int>(type: "int", nullable: false),
                    Updated_By = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Entry_Date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Entry_Time = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trace_Scrap_Entry", x => x.Scrap_EntryId);
                });

            migrationBuilder.CreateTable(
                name: "Trace_Status_Master",
                columns: table => new
                {
                    Status_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Status_Code = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    Status_Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trace_Status_Master", x => x.Status_Id);
                });

            migrationBuilder.CreateTable(
                name: "Trace_Storage_Location_Master",
                columns: table => new
                {
                    Storage_Loc_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ShopFloorLocation_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Storage_Loc_Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trace_Storage_Location_Master", x => x.Storage_Loc_Id);
                });

            migrationBuilder.CreateTable(
                name: "Trace_Tag_Generation",
                columns: table => new
                {
                    TagId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Location_Id = table.Column<int>(type: "int", nullable: false),
                    Printer_Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Printer_Ip = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Printed_DateTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Module_Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    User_Id = table.Column<int>(type: "int", nullable: true),
                    Print_Status = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trace_Tag_Generation", x => x.TagId);
                });

            migrationBuilder.CreateTable(
                name: "Trace_Taken_ToTB",
                columns: table => new
                {
                    TakenToTb_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Production_Id = table.Column<int>(type: "int", nullable: false),
                    Ageing_Id = table.Column<int>(type: "int", nullable: true),
                    TakenToTb_Date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    TakenToTB_Time = table.Column<DateTime>(type: "datetime2", nullable: true),
                    TakenToTB_By = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    TakenToTb_Status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trace_Taken_ToTB", x => x.TakenToTb_Id);
                });

            migrationBuilder.CreateTable(
                name: "Trace_User_Type",
                columns: table => new
                {
                    User_Type_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    User_Type_Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    User_Type_Status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trace_User_Type", x => x.User_Type_Id);
                });

            migrationBuilder.CreateTable(
                name: "Trace_UWB_Anchor_Master",
                columns: table => new
                {
                    UWB_Anchor_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UWD_Anchor_Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UWD_Anchor_Make = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UWD_Anchor_Installed_Date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UWD_Anchor_Status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trace_UWB_Anchor_Master", x => x.UWB_Anchor_Id);
                });

            migrationBuilder.CreateTable(
                name: "Trace_UWB_Tag_Master",
                columns: table => new
                {
                    UWB_Tag_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UWB_Tag_Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UWB_Tag_Uniqe_Id = table.Column<int>(type: "int", nullable: true),
                    UWB_Tag_Status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trace_UWB_Tag_Master", x => x.UWB_Tag_Id);
                });

            migrationBuilder.CreateTable(
                name: "Trace_WorkCenter_Master",
                columns: table => new
                {
                    WorkCent_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    WorkCent_Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Plant_Id = table.Column<int>(type: "int", nullable: false),
                    WorkCent_Type = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trace_WorkCenter_Master", x => x.WorkCent_Id);
                });

            migrationBuilder.CreateTable(
                name: "FGS_FIFO_Customer_Master",
                columns: table => new
                {
                    Customer_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Customer_Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Customer_Address = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Batch_Type_Id = table.Column<int>(type: "int", nullable: false),
                    Customer_Type_Id = table.Column<int>(type: "int", nullable: false),
                    Cust_Status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FGS_FIFO_Customer_Master", x => x.Customer_Id);
                    table.ForeignKey(
                        name: "FK_FGS_FIFO_Customer_Master_FGS_FIFO_Batch_Type_Master_Batch_Type_Id",
                        column: x => x.Batch_Type_Id,
                        principalTable: "FGS_FIFO_Batch_Type_Master",
                        principalColumn: "Batch_Type_Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FGS_FIFO_Customer_Master_FGS_FIFO_Customer_Type_Master_Customer_Type_Id",
                        column: x => x.Customer_Type_Id,
                        principalTable: "FGS_FIFO_Customer_Type_Master",
                        principalColumn: "Customer_Type_Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FGS_FIFO_Material_Master",
                columns: table => new
                {
                    Material_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Material_Code = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Material_Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Material_Type_Id = table.Column<int>(type: "int", nullable: false),
                    EntryDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Material_Status = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FGS_FIFO_Material_Master", x => x.Material_Id);
                    table.ForeignKey(
                        name: "FK_FGS_FIFO_Material_Master_FGS_FIFO_Material_Type_Master_Material_Type_Id",
                        column: x => x.Material_Type_Id,
                        principalTable: "FGS_FIFO_Material_Type_Master",
                        principalColumn: "Material_Type_Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Trace_Application_Employee_Mapping",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Module_Id = table.Column<int>(type: "int", nullable: false),
                    LogMastEmp_Id = table.Column<int>(type: "int", nullable: false),
                    View = table.Column<int>(type: "int", nullable: false),
                    Update = table.Column<int>(type: "int", nullable: false),
                    Delete = table.Column<int>(type: "int", nullable: false),
                    Add = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trace_Application_Employee_Mapping", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Trace_Application_Employee_Mapping_Trace_Login_Master_LogMastEmp_Id",
                        column: x => x.LogMastEmp_Id,
                        principalTable: "Trace_Login_Master",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Trace_Application_Employee_Mapping_Trace_Module_Master_Module_Id",
                        column: x => x.Module_Id,
                        principalTable: "Trace_Module_Master",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FGS_FIFO_Cust_Ageing_Mapping",
                columns: table => new
                {
                    Cust_Ageing_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Customer_Id = table.Column<int>(type: "int", nullable: false),
                    Ageing_Id = table.Column<int>(type: "int", nullable: false),
                    EntryDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Ageing_Status = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FGS_FIFO_Cust_Ageing_Mapping", x => x.Cust_Ageing_Id);
                    table.ForeignKey(
                        name: "FK_FGS_FIFO_Cust_Ageing_Mapping_FGS_FIFO_Ageing_Master_Ageing_Id",
                        column: x => x.Ageing_Id,
                        principalTable: "FGS_FIFO_Ageing_Master",
                        principalColumn: "Ageing_Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FGS_FIFO_Cust_Ageing_Mapping_FGS_FIFO_Customer_Master_Customer_Id",
                        column: x => x.Customer_Id,
                        principalTable: "FGS_FIFO_Customer_Master",
                        principalColumn: "Customer_Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FGS_FIFO_Daily_Receipt_Transaction",
                columns: table => new
                {
                    Inv_TransId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Barcode = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Supplier_Id = table.Column<int>(type: "int", nullable: false),
                    Material_Id = table.Column<int>(type: "int", nullable: false),
                    Batch_Type_Id = table.Column<int>(type: "int", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    Invoice_No = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Invoice_Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Location_Id = table.Column<int>(type: "int", nullable: false),
                    ProdDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Exp_Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    EntryDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Entered_By = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FGS_FIFO_Daily_Receipt_Transaction", x => x.Inv_TransId);
                    table.ForeignKey(
                        name: "FK_FGS_FIFO_Daily_Receipt_Transaction_FGS_FIFO_Batch_Type_Master_Batch_Type_Id",
                        column: x => x.Batch_Type_Id,
                        principalTable: "FGS_FIFO_Batch_Type_Master",
                        principalColumn: "Batch_Type_Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FGS_FIFO_Daily_Receipt_Transaction_FGS_FIFO_Location_Master_Location_Id",
                        column: x => x.Location_Id,
                        principalTable: "FGS_FIFO_Location_Master",
                        principalColumn: "Location_Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FGS_FIFO_Daily_Receipt_Transaction_FGS_FIFO_Material_Master_Material_Id",
                        column: x => x.Material_Id,
                        principalTable: "FGS_FIFO_Material_Master",
                        principalColumn: "Material_Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FGS_FIFO_Daily_Receipt_Transaction_FGS_FIFO_Supplier_Master_Supplier_Id",
                        column: x => x.Supplier_Id,
                        principalTable: "FGS_FIFO_Supplier_Master",
                        principalColumn: "Supplier_Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FGS_FIFO_Despatch_Request",
                columns: table => new
                {
                    Desp_Req_Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Material_Id = table.Column<int>(type: "int", nullable: false),
                    Customer_id = table.Column<int>(type: "int", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    Requested_DateTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Entered_By = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Invoice_No = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Invoice_Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Request_Status = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FGS_FIFO_Despatch_Request", x => x.Desp_Req_Id);
                    table.ForeignKey(
                        name: "FK_FGS_FIFO_Despatch_Request_FGS_FIFO_Customer_Master_Customer_id",
                        column: x => x.Customer_id,
                        principalTable: "FGS_FIFO_Customer_Master",
                        principalColumn: "Customer_Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FGS_FIFO_Despatch_Request_FGS_FIFO_Material_Master_Material_Id",
                        column: x => x.Material_Id,
                        principalTable: "FGS_FIFO_Material_Master",
                        principalColumn: "Material_Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FGS_FIFO_Daily_Despatch_Transaction",
                columns: table => new
                {
                    Desp_TransId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Desp_Req_Id = table.Column<int>(type: "int", nullable: false),
                    Inv_TransId = table.Column<int>(type: "int", nullable: false),
                    Quantity_Despatched = table.Column<int>(type: "int", nullable: false),
                    EntryDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Entered_By = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Desp_Status_Id = table.Column<int>(type: "int", nullable: false),
                    vehicle_number = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Barcode = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FGS_FIFO_Daily_Despatch_Transaction", x => x.Desp_TransId);
                    table.ForeignKey(
                        name: "FK_FGS_FIFO_Daily_Despatch_Transaction_FGS_FIFO_Despatch_Request_Desp_Req_Id",
                        column: x => x.Desp_Req_Id,
                        principalTable: "FGS_FIFO_Despatch_Request",
                        principalColumn: "Desp_Req_Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FGS_FIFO_Daily_Despatch_Transaction_FGS_FIFO_Despatch_Status_Master_Desp_Status_Id",
                        column: x => x.Desp_Status_Id,
                        principalTable: "FGS_FIFO_Despatch_Status_Master",
                        principalColumn: "Desp_Status_Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FGS_FIFO_Cust_Ageing_Mapping_Ageing_Id",
                table: "FGS_FIFO_Cust_Ageing_Mapping",
                column: "Ageing_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FGS_FIFO_Cust_Ageing_Mapping_Customer_Id",
                table: "FGS_FIFO_Cust_Ageing_Mapping",
                column: "Customer_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FGS_FIFO_Customer_Master_Batch_Type_Id",
                table: "FGS_FIFO_Customer_Master",
                column: "Batch_Type_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FGS_FIFO_Customer_Master_Customer_Type_Id",
                table: "FGS_FIFO_Customer_Master",
                column: "Customer_Type_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FGS_FIFO_Daily_Despatch_Transaction_Desp_Req_Id",
                table: "FGS_FIFO_Daily_Despatch_Transaction",
                column: "Desp_Req_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FGS_FIFO_Daily_Despatch_Transaction_Desp_Status_Id",
                table: "FGS_FIFO_Daily_Despatch_Transaction",
                column: "Desp_Status_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FGS_FIFO_Daily_Receipt_Transaction_Batch_Type_Id",
                table: "FGS_FIFO_Daily_Receipt_Transaction",
                column: "Batch_Type_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FGS_FIFO_Daily_Receipt_Transaction_Location_Id",
                table: "FGS_FIFO_Daily_Receipt_Transaction",
                column: "Location_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FGS_FIFO_Daily_Receipt_Transaction_Material_Id",
                table: "FGS_FIFO_Daily_Receipt_Transaction",
                column: "Material_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FGS_FIFO_Daily_Receipt_Transaction_Supplier_Id",
                table: "FGS_FIFO_Daily_Receipt_Transaction",
                column: "Supplier_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FGS_FIFO_Despatch_Request_Customer_id",
                table: "FGS_FIFO_Despatch_Request",
                column: "Customer_id");

            migrationBuilder.CreateIndex(
                name: "IX_FGS_FIFO_Despatch_Request_Material_Id",
                table: "FGS_FIFO_Despatch_Request",
                column: "Material_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FGS_FIFO_Material_Master_Material_Type_Id",
                table: "FGS_FIFO_Material_Master",
                column: "Material_Type_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Trace_Application_Employee_Mapping_LogMastEmp_Id",
                table: "Trace_Application_Employee_Mapping",
                column: "LogMastEmp_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Trace_Application_Employee_Mapping_Module_Id",
                table: "Trace_Application_Employee_Mapping",
                column: "Module_Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FGS_FIFO_Cust_Ageing_Mapping");

            migrationBuilder.DropTable(
                name: "FGS_FIFO_Daily_Despatch_Transaction");

            migrationBuilder.DropTable(
                name: "FGS_FIFO_Daily_Receipt_Transaction");

            migrationBuilder.DropTable(
                name: "Trace_Application_Employee_Mapping");

            migrationBuilder.DropTable(
                name: "Trace_Application_Master");

            migrationBuilder.DropTable(
                name: "Trace_Barcode_Generation");

            migrationBuilder.DropTable(
                name: "Trace_Hold");

            migrationBuilder.DropTable(
                name: "Trace_Hold_Reasons");

            migrationBuilder.DropTable(
                name: "Trace_Hold_Release");

            migrationBuilder.DropTable(
                name: "Trace_Login_Details");

            migrationBuilder.DropTable(
                name: "Trace_Material_Consumed_Actual");

            migrationBuilder.DropTable(
                name: "Trace_Material_Consumed_Spec");

            migrationBuilder.DropTable(
                name: "Trace_Material_Master");

            migrationBuilder.DropTable(
                name: "Trace_Material_Transaction");

            migrationBuilder.DropTable(
                name: "Trace_MHE_Master");

            migrationBuilder.DropTable(
                name: "Trace_Plant_Master");

            migrationBuilder.DropTable(
                name: "Trace_Print_Generation");

            migrationBuilder.DropTable(
                name: "Trace_Prod_Remarks");

            migrationBuilder.DropTable(
                name: "Trace_Product_Ageing");

            migrationBuilder.DropTable(
                name: "Trace_Production");

            migrationBuilder.DropTable(
                name: "Trace_Scrap_Entry");

            migrationBuilder.DropTable(
                name: "Trace_Status_Master");

            migrationBuilder.DropTable(
                name: "Trace_Storage_Location_Master");

            migrationBuilder.DropTable(
                name: "Trace_Tag_Generation");

            migrationBuilder.DropTable(
                name: "Trace_Taken_ToTB");

            migrationBuilder.DropTable(
                name: "Trace_User_Type");

            migrationBuilder.DropTable(
                name: "Trace_UWB_Anchor_Master");

            migrationBuilder.DropTable(
                name: "Trace_UWB_Tag_Master");

            migrationBuilder.DropTable(
                name: "Trace_WorkCenter_Master");

            migrationBuilder.DropTable(
                name: "FGS_FIFO_Ageing_Master");

            migrationBuilder.DropTable(
                name: "FGS_FIFO_Despatch_Request");

            migrationBuilder.DropTable(
                name: "FGS_FIFO_Despatch_Status_Master");

            migrationBuilder.DropTable(
                name: "FGS_FIFO_Location_Master");

            migrationBuilder.DropTable(
                name: "FGS_FIFO_Supplier_Master");

            migrationBuilder.DropTable(
                name: "Trace_Login_Master");

            migrationBuilder.DropTable(
                name: "Trace_Module_Master");

            migrationBuilder.DropTable(
                name: "FGS_FIFO_Customer_Master");

            migrationBuilder.DropTable(
                name: "FGS_FIFO_Material_Master");

            migrationBuilder.DropTable(
                name: "FGS_FIFO_Batch_Type_Master");

            migrationBuilder.DropTable(
                name: "FGS_FIFO_Customer_Type_Master");

            migrationBuilder.DropTable(
                name: "FGS_FIFO_Material_Type_Master");
        }
    }
}
