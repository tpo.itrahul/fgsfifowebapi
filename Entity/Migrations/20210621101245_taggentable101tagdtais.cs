﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Entity.Migrations
{
    public partial class taggentable101tagdtais : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "InvTranId",
                table: "Trace_Tag_Generation",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TagDetails",
                table: "Trace_Tag_Generation",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InvTranId",
                table: "Trace_Tag_Generation");

            migrationBuilder.DropColumn(
                name: "TagDetails",
                table: "Trace_Tag_Generation");
        }
    }
}
