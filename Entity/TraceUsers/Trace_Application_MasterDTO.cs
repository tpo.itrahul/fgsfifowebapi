﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TraceUsers
{
    public class Trace_Application_MasterDTO
    {
    
        public int Id { get; set; }
        public int Application_Id { get; set; }
        public string Application_Name { get; set; }
        public int Application_Status { get; set; }

        public int Plant_Id { get; set; }
    }
}
