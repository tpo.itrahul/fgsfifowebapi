﻿using Model.TraceUsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TraceUsers
{
  public  class UserAccessDTO
    {
        public int EmpId { get; set; }
        public string UserRole { get; set; }
        public int PlantID { get; set; }
        public int ModuleId { get; set; }
        public ICollection<Trace_Application_Employee_Mapping> ICAppEmpMapRelation { get; set; }

    }
}
