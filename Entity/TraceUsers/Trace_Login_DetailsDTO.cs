﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TraceUsers
{
    public class Trace_Login_DetailsDTO
    {
    
       public int Id { get; set; }
        public int Emp_Id { get; set; }
        public int Module_Id { get; set; }
        public int Logged_Time { get; set; }
        public int Plant_Id { get; set; }

    }
}
