﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.FGS_FIFO;
using Model.Tread_FIFO;
using Repository;
using Entity.TreadFIFO;
namespace TraceMe.Controllers.Tread_FIFO
{
    [Route("api/[controller]")]
    [ApiController]
    public class Trace_Hold_ReleaseController : ControllerBase
    {

        private IRepositoryWrapper _repoWrapper;
        public Trace_Hold_ReleaseController(IRepositoryWrapper repowrapper)
        {
            _repoWrapper = repowrapper;

        }

        // GET: api/Trace_Hold_Release
        [HttpGet]
        public async Task<IActionResult> GetTrace_Hold_Release()
        {

            try
            {
                var result = await _repoWrapper.IHoldRelease.GetTrace_Hold_Release();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }

        // GET: api/Trace_Hold_Release/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrace_Hold_Release(int id)
        {
            try
            {
                var trace_Hold_Release = await _repoWrapper.IHoldRelease.GetTrace_Hold_Release((id));
                if (trace_Hold_Release == null)
                {
                    return NotFound();
                }
                return Ok(trace_Hold_Release);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }

        // PUT: api/Trace_Hold_Release/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrace_Hold_Release(int id, TraceHoldReleaseDTO trace_Hold_Release)
        {
            if (id != trace_Hold_Release.Hold_Release_Id)
            {
                return BadRequest();
            }



            try
            {
                if (trace_Hold_Release == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.ITakenToTB.GetTrace_Taken_ToTB(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IHoldRelease.UpdateTrace_Hold_Release(trace_Hold_Release);
                await _repoWrapper.SaveAsync();
                return Ok(trace_Hold_Release);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


        }

        // POST: api/Trace_Hold_Release
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<IActionResult> PostTrace_Hold_Release(TraceHoldReleaseDTO trace_Hold_Release)
        {
            try
            {
                if (trace_Hold_Release == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.IHoldRelease.CreateTrace_Hold(trace_Hold_Release);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetTrace_Hold_Release", new { id = trace_Hold_Release.Hold_Release_Id }, trace_Hold_Release);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }

        // DELETE: api/Trace_Hold_Release/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_Hold_Release(int id)
        {
            try
            {
                var ifexist = _repoWrapper.IHoldRelease.GetTrace_Hold_Release(id);
                if (ifexist == null)
                {

                    return NotFound();
                }

                _repoWrapper.IHoldRelease.DeleteTrace_Hold_Release(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }



        }
    }
}
