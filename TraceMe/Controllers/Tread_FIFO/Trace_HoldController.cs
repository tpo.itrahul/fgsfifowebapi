﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.FGS_FIFO;
using Model.Tread_FIFO;
using Repository;
using Entity.TreadFIFO;
namespace TraceMe.Controllers.Tread_FIFO
{
    [Route("api/[controller]")]
    [ApiController]
    public class Trace_HoldController : ControllerBase
    {

        private IRepositoryWrapper _repoWrapper;
        public Trace_HoldController(Trace_DBContext context, IRepositoryWrapper repowrapper)
        {
            _repoWrapper = repowrapper;

        }

        // GET: api/Trace_Hold
        [HttpGet]
        public async Task<IActionResult> GetTrace_Hold()
        {
            try
            {
                var result = await _repoWrapper.IHold.GetTrace_Hold();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET: api/Trace_Hold/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Trace_Hold>> GetTrace_Hold(int id)
        {
            try
            {
                var trace_Hold = await _repoWrapper.IHold.GetTrace_Hold((id));
                if (trace_Hold == null)
                {
                    return NotFound();
                }
                return Ok(trace_Hold);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }

        // PUT: api/Trace_Hold/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrace_Hold(int id, TraceHoldDTO trace_Hold)
        {
            if (id != trace_Hold.Hold_Id)
            {
                return BadRequest();
            }


            try
            {
                if (trace_Hold == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.IHold.GetTrace_Hold(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IHold.UpdateTrace_Hold(trace_Hold);
                await _repoWrapper.SaveAsync();
                return Ok(trace_Hold);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }

        // POST: api/Trace_Hold
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<IActionResult> PostTrace_Hold(TraceHoldDTO trace_Hold)
        {

            try
            {
                if (trace_Hold == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.IHold.CreateTrace_Hold(trace_Hold);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetTrace_Taken_ToTB", new { id = trace_Hold.Hold_Id }, trace_Hold);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


        }

        // DELETE: api/Trace_Hold/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_Hold(int id)
        {
            try
            {
                var ifexist = _repoWrapper.IHold.GetTrace_Hold(id);
                if (ifexist == null)
                {

                    return NotFound();
                }

                _repoWrapper.IHold.DeleteTrace_Hold(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }
    }
}
