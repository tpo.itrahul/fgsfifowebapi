﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.FGS_FIFO;
using Model.Tread_FIFO;
using Repository;
using Entity.TreadFIFO;
namespace TraceMe.Controllers.Tread_FIFO
{
    [Route("api/[controller]")]
    [ApiController]
    public class Trace_Hold_ReasonsController : ControllerBase
    {
       
        private IRepositoryWrapper _repoWrapper;
        public Trace_Hold_ReasonsController(Trace_DBContext context, IRepositoryWrapper repowrapper)
        {
            _repoWrapper = repowrapper;
          
        }

        // GET: api/Trace_Hold_Reasons
        [HttpGet]
        public async Task<IActionResult> GetTrace_Hold_Reasons()
        {
            try
            {
                var result = await _repoWrapper.IHoldReasons.GetTrace_Hold_Reasons();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET: api/Trace_Hold_Reasons/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrace_Hold_Reasons(int id)
        {
            try
            {
                var trace_Hold_Reasons = await _repoWrapper.IHoldReasons.GetTrace_Hold_Reasons((id));
                if (trace_Hold_Reasons == null)
                {
                    return NotFound();
                }
                return Ok(trace_Hold_Reasons);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


        
        }

        // PUT: api/Trace_Hold_Reasons/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrace_Hold_Reasons(int id, TraceHoldReasonsDTO trace_Hold_Reasons)
        {
            if (id != trace_Hold_Reasons.Hold_Reason_Id)
            {
                return BadRequest();
            }

          
            try
            {
                if (trace_Hold_Reasons == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.IHoldReasons.GetTrace_Hold_Reasons(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IHoldReasons.UpdateTrace_Hold_Reasons(trace_Hold_Reasons);
                await _repoWrapper.SaveAsync();
                return Ok(trace_Hold_Reasons);


            }

            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

            return NoContent();
        }

        // POST: api/Trace_Hold_Reasons
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<IActionResult> PostTrace_Hold_Reasons([FromBody] TraceHoldReasonsDTO trace_Hold_Reasons)
        {

            try
            {
                if (trace_Hold_Reasons == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.IHoldReasons.CreateTrace_Hold_Reasons(trace_Hold_Reasons);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetTrace_Hold_Reasons", new { id = trace_Hold_Reasons.Hold_Reason_Id }, trace_Hold_Reasons);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
     
         
        }

        // DELETE: api/Trace_Hold_Reasons/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_Hold_Reasons(int id)
        {
            try
            {
                var ifexist = _repoWrapper.IHoldReasons.GetTrace_Hold_Reasons(id);
                if (ifexist == null)
                {

                    return NotFound();
                }

                _repoWrapper.IHoldReasons.DeleteTrace_Hold_Reasons(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }


    }
}
