﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.FGS_FIFO;
using Model.Tread_FIFO;
using Repository;
using Entity.TreadFIFO;
namespace TraceMe.Controllers.Tread_FIFO
{
    [Route("api/[controller]")]
    [ApiController]
    public class Trace_Taken_ToTBController : ControllerBase
    {
       
        private IRepositoryWrapper _repoWrapper;
        public Trace_Taken_ToTBController( IRepositoryWrapper repowrapper)
        {
            _repoWrapper = repowrapper;
           
        }

        // GET: api/Trace_Taken_ToTB
        [HttpGet]
        public async Task<IActionResult> GetTrace_Taken_ToTB()
        {
            try
            {
                var result = await _repoWrapper.ITakenToTB.GetTrace_Taken_ToTB();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
          
        }

        // GET: api/Trace_Taken_ToTB/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrace_Taken_ToTB(int id)
        {           
            
            try
            {
                var trace_Taken_ToTB =await _repoWrapper.ITakenToTB.GetTrace_Taken_ToTB((id));
                if (trace_Taken_ToTB == null)
                {
                    return NotFound();
                }
                return Ok(trace_Taken_ToTB);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }

        // PUT: api/Trace_Taken_ToTB/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrace_Taken_ToTB(int id, TraceTakenToTbDTO trace_Taken_ToTB)
        {
            if (id != trace_Taken_ToTB.TakenToTb_Id)
            {
                return BadRequest();
            }
            try
            {
                if (trace_Taken_ToTB == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.ITakenToTB.GetTrace_Taken_ToTB(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.ITakenToTB.UpdateTrace_Taken_ToTB(trace_Taken_ToTB);
                await _repoWrapper.SaveAsync();
                return Ok(trace_Taken_ToTB);
            }

            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }

      
        [HttpPost]
        public async Task<IActionResult> PostTrace_Taken_ToTB([FromBody] TraceTakenToTbDTO trace_Taken_ToTB)
        {
            try
            {
                if (trace_Taken_ToTB == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.ITakenToTB.CreateTrace_Taken_ToTB(trace_Taken_ToTB);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetTrace_Taken_ToTB", new { id = trace_Taken_ToTB.TakenToTb_Id }, trace_Taken_ToTB);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
          

           
        }

        // DELETE: api/Trace_Taken_ToTB/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_Taken_ToTB(int id)
        {

            try
            {
                var ifexist = _repoWrapper.ITakenToTB.GetTrace_Taken_ToTB(id);
                if (ifexist == null)
                {

                    return NotFound();
                }

                _repoWrapper.ITakenToTB.DeleteTrace_Taken_ToTB(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }
            

    }
}
