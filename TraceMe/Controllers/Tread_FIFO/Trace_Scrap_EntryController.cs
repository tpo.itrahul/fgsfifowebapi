﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.FGS_FIFO;
using Model.Tread_FIFO;
using Repository;
using Entity.TreadFIFO;
namespace TraceMe.Controllers.Tread_FIFO
{
    [Route("api/[controller]")]
    [ApiController]
    public class Trace_Scrap_EntryController : ControllerBase
    {
       
        private IRepositoryWrapper _repoWrapper;
        public Trace_Scrap_EntryController( IRepositoryWrapper repowrapper)
        {
            _repoWrapper = repowrapper;
           
        }

        // GET: api/Trace_Scrap_Entry
        [HttpGet]
        public async Task<IActionResult> GetTrace_Scrap_Entry()
        {
            try
            {
                var result = await _repoWrapper.IScrapEntry.GetTrace_Scrap_Entry();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
           
        }

        // GET: api/Trace_Scrap_Entry/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrace_Scrap_Entry(int id)
        {

            try
            {
                var trace_Scrap_Entry =await _repoWrapper.IScrapEntry.GetTrace_Scrap_Entry(id);
                if (trace_Scrap_Entry == null)
                {
                    return NotFound();
                }
                return Ok(trace_Scrap_Entry);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }

        // PUT: api/Trace_Scrap_Entry/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrace_Scrap_Entry(int id, TraceScrapEntryDTO trace_Scrap_Entry)
        {
            if (id != trace_Scrap_Entry.Scrap_EntryId)
            {
                return BadRequest();
            }
                  

            try
            {
                if (trace_Scrap_Entry == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.IScrapEntry.GetTrace_Scrap_Entry(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IScrapEntry.UpdateTrace_Scrap_Entry(trace_Scrap_Entry);
                await _repoWrapper.SaveAsync();
                return Ok(trace_Scrap_Entry);


            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


        }

        // POST: api/Trace_Scrap_Entry
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<IActionResult> PostTrace_Scrap_Entry([FromBody] TraceScrapEntryDTO trace_Scrap_Entry)
        {
            try
            {
                if (trace_Scrap_Entry == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest("Owner object is null");
                }
                _repoWrapper.IScrapEntry.CreateTrace_Scrap_Entry(trace_Scrap_Entry);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetTrace_Scrap_Entry", new { id = trace_Scrap_Entry.Scrap_EntryId }, trace_Scrap_Entry);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

           
        }

      
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_Scrap_Entry(int id)
        {

            try
            {
                var ifexist = _repoWrapper.IScrapEntry.GetTrace_Scrap_Entry(id);
                if (ifexist == null)
                {

                    return NotFound();
                }

                _repoWrapper.IScrapEntry.DeleteTrace_Scrap_Entry(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }

    }
}
