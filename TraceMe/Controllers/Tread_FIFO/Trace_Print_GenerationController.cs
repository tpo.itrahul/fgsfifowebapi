﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.FGS_FIFO;
using Model.Tread_FIFO;
using Repository;
using Entity.TreadFIFO;
namespace TraceMe.Controllers.Tread_FIFO
{
    [Route("api/[controller]")]
    [ApiController]
    public class Trace_Print_GenerationController : ControllerBase
    {
      
        private IRepositoryWrapper _repoWrapper;
        public Trace_Print_GenerationController( IRepositoryWrapper repowrapper)
        {
            _repoWrapper = repowrapper;
           
        }

        // GET: api/Trace_Print_Generation
        [HttpGet]
        public async Task<IActionResult> GetTrace_Print_Generation()
        {
            try
            {
                var result = await _repoWrapper.IPrintGeneration.GetTrace_PrintGeneration();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
           
        }

        // GET: api/Trace_Print_Generation/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrace_Print_Generation(int id)
        {
            try
            {
                var trace_Print_Generation = await _repoWrapper.IPrintGeneration.GetTrace_PrintGeneration((id));
                if (trace_Print_Generation == null)
                {
                    return NotFound();
                }
                return Ok(trace_Print_Generation);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
       
        }

        // PUT: api/Trace_Print_Generation/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrace_Print_Generation(int id, TracePrintGenerationDTO trace_Print_Generation)
        {
            if (id != trace_Print_Generation.Print_Id)
            {
                return BadRequest();
            }

           

            try
            {
                if (trace_Print_Generation == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.ITakenToTB.GetTrace_Taken_ToTB(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IPrintGeneration.UpdateTrace_PrintGeneration(trace_Print_Generation);
                await _repoWrapper.SaveAsync();
                return Ok(trace_Print_Generation);


            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }



        }

        // POST: api/Trace_Print_Generation
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<IActionResult> PostTrace_Print_Generation([FromBody] TracePrintGenerationDTO trace_Print_Generation)
        {
            try
            {
                if (trace_Print_Generation == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.IPrintGeneration.CreateTrace_PrintGeneration(trace_Print_Generation);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetTrace_Taken_ToTB", new { id = trace_Print_Generation.Print_Id }, trace_Print_Generation);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
          
        }

        // DELETE: api/Trace_Print_Generation/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_Print_Generation(int id)
        {
            try
            {
                var ifexist = _repoWrapper.IPrintGeneration.GetTrace_PrintGeneration(id);
                if (ifexist == null)
                {

                    return NotFound();
                }

                _repoWrapper.IPrintGeneration.DeleteTrace_PrintGeneration(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            };
        }

        
    }
}
