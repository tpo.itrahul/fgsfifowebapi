﻿using System;

using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using Entity.TraceUsers;

using Repository;
namespace TraceMe.Controllers.Trace_Users
{
    [Route("api/[controller]")]

    public class Trace_Login_MasterController : ControllerBase
    {
        
        private IRepositoryWrapper _repoWrapper;
        public Trace_Login_MasterController( IRepositoryWrapper repowrapper)
        {
            _repoWrapper = repowrapper;
          
        }

        // GET: api/Trace_Production
        [HttpGet]
        public async Task<IActionResult> GetTrace_Login_Master()
        {
            try
            {
                var result = await _repoWrapper.ILoginMaster.GetTrace_Login_Master();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET: api/Trace_Production/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrace_Login_Master(int id)
        {

            try
            {
                var trace_loginMaster = await _repoWrapper.ILoginMaster.GetTrace_Login_Master(id);
                if (trace_loginMaster == null)
                {
                    return NotFound();
                }
                return Ok(trace_loginMaster);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        
        }

        // PUT: api/Trace_Production/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrace_Login_Master(int id, Trace_Login_MasterDTO trace_loginMaster)
        {
            if (id != trace_loginMaster.Id)
            {
                return BadRequest();
            }

          

            try
            {
                if (trace_loginMaster == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.ILoginMaster.GetTrace_Login_Master(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.ILoginMaster.UpdateTrace_Login_Master(trace_loginMaster);
                await _repoWrapper.SaveAsync();
                return Ok(trace_loginMaster);

            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");

            }
        }

            
        

        // POST: api/Trace_Production
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<IActionResult> PostTrace_Application_Login_Master(Trace_Login_MasterDTO trace_loginMaster)
        {
            try
            {
                if (trace_loginMaster == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.ILoginMaster.CreateTrace_Login_Master(trace_loginMaster);
                await _repoWrapper.SaveAsync();
                // return CreatedAtAction("GetTrace_Production", new { id = trace_loginMaster.Id }, trace_loginMaster);
                return Ok("Succees");
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

         
        }

        // DELETE: api/Trace_Production/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_Application_Login_Master(int id)
        {

            try
            {
                var ifexist = _repoWrapper.ILoginMaster.GetTrace_Login_Master(id);
                if (ifexist == null)
                {

                    return NotFound();
                }

                _repoWrapper.ILoginMaster.DeleteTrace_Login_Master(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET: api/Trace_Production/5
        [HttpGet("Trace_ValidateCredentials/{username}/{password}")]
        public async Task<IActionResult> Trace_ValidateCredentials(string username, string password)
        {

            try
            {
                var trace_loginMaster =  _repoWrapper.ILoginMaster.ValidateLogin(username, password);
                if (trace_loginMaster == null)
                {
                    return NotFound();
                }

            //  var App_acess = await _repoWrapper.IApplicationEmployeeMapping.GetTrace_Application_Employee_Mapping(trace_loginMaster.Id);


                return Ok(trace_loginMaster);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


        }


    }
}
