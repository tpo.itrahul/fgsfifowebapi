﻿using System;

using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using Entity.TraceTransactions;

using Repository;
namespace TraceMe.Controllers.Trace_Transactions
{
    [Route("api/[controller]")]
    [ApiController]
    public class Trace_Material_Consumed_SpecController : ControllerBase
    {
      
        private IRepositoryWrapper _repoWrapper;
        public Trace_Material_Consumed_SpecController( IRepositoryWrapper repowrapper)
        {
            _repoWrapper = repowrapper;
          
        }

        // GET: api/Trace_Material_Consumed_Spec
        [HttpGet]
        public async Task<IActionResult> GetTrace_Material_Consumed_Spec()
        {
            try
            {
                var result = await _repoWrapper.IMaterialConsumedSpec.GetTrace_Material_Consumed_Spec();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
           
        }

        // GET: api/Trace_Material_Consumed_Spec/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrace_Material_Consumed_Spec(int id)
        {

            try
            {
                var trace_Material_Consumed_Spec = await _repoWrapper.IMaterialConsumedSpec.GetTrace_Material_Consumed_Spec((id));
                if (trace_Material_Consumed_Spec == null)
                {
                    return NotFound();
                }
                return Ok(trace_Material_Consumed_Spec);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


           
        }

        // PUT: api/Trace_Material_Consumed_Spec/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrace_Material_Consumed_Spec(int id, TraceMaterialConsumedSpecDTO trace_Material_Consumed_Spec)
        {
            if (id != trace_Material_Consumed_Spec.MCS_Id)
            {
                return BadRequest();
            }

           

            try
            {
                if (trace_Material_Consumed_Spec == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.IMaterialConsumedSpec.GetTrace_Material_Consumed_Spec(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IMaterialConsumedSpec.UpdateTrace_Material_Consumed_Spec(trace_Material_Consumed_Spec);
                await _repoWrapper.SaveAsync();
                return Ok(trace_Material_Consumed_Spec);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

            // return NoContent();
        }

        // POST: api/Trace_Material_Consumed_Spec
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<IActionResult> PostTrace_Material_Consumed_Spec([FromBody] TraceMaterialConsumedSpecDTO trace_Material_Consumed_Spec)
        {
            try
            {
                if (trace_Material_Consumed_Spec == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.IMaterialConsumedSpec.CreateTrace_Material_Consumed_Spec(trace_Material_Consumed_Spec);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetTrace_Material_Consumed_Spec", new { id = trace_Material_Consumed_Spec.MCS_Id }, trace_Material_Consumed_Spec);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

           
        }

        // DELETE: api/Trace_Material_Consumed_Spec/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_Material_Consumed_Spec(int id)
        {
            try
            {
                var ifexist = _repoWrapper.IMaterialConsumedSpec.GetTrace_Material_Consumed_Spec(id);
                if (ifexist == null)
                {

                    return NotFound();
                }

                _repoWrapper.IMaterialConsumedSpec.DeleteTrace_Material_Consumed_Spec(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }


    }
}
