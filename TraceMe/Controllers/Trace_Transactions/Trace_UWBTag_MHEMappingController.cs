﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.TraceTransactions;
using Model.Trace_Transactions;
using Repository;
namespace TraceMe.Controllers.Trace_Transactions
{
    [Route("api/[controller]")]
    [ApiController]
    public class Trace_UWBTag_MHEMappingController : ControllerBase
    {

        private IRepositoryWrapper _repoWrapper;
        public Trace_UWBTag_MHEMappingController(IRepositoryWrapper repowrapper)
        {
            _repoWrapper = repowrapper;

        }

        // GET: api/Trace_UWBTag_MHEMapping
        [HttpGet]
        public async Task<IActionResult> GetTrace_UWBTag_MHEMapping()
        {
            try
            {
                var result = await _repoWrapper.IUWBTagMHEMapping.GetTrace_TraceUWBTAgMHEMapping();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET: api/Trace_UWBTag_MHEMapping/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrace_UWBTag_MHEMapping(int id)
        {

            try
            {
                var trace_UWBTag_MHEMapping = await _repoWrapper.IUWBTagMHEMapping.GetTrace_TraceUWBTAgMHEMapping((id));
                if (trace_UWBTag_MHEMapping == null)
                {
                    return NotFound();
                }
                return Ok(trace_UWBTag_MHEMapping);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


        }

        // PUT: api/Trace_UWBTag_MHEMapping/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrace_UWBTag_MHEMapping(int id, TraceUWBTagMHEMappingDTO trace_UWBTag_MHEMapping)
        {
            if (id != trace_UWBTag_MHEMapping.Tag_MHE_Id)
            {
                return BadRequest();
            }


            try
            {
                if (trace_UWBTag_MHEMapping == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.IUWBTagMHEMapping.GetTrace_TraceUWBTAgMHEMapping(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IUWBTagMHEMapping.UpdateTrace_TraceUWBTAgMHEMapping(trace_UWBTag_MHEMapping);
                await _repoWrapper.SaveAsync();
                return Ok(trace_UWBTag_MHEMapping);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


        }

        // POST: api/Trace_UWBTag_MHEMapping
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<IActionResult> PostTrace_UWBTag_MHEMapping(TraceUWBTagMHEMappingDTO trace_UWBTag_MHEMapping)
        {
            try
            {
                if (trace_UWBTag_MHEMapping == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
              //  _repoWrapper.IUWBTagMHEMapping.CreateTrace_UWBTag_MHEMapping(trace_UWBTag_MHEMapping);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }

        // DELETE: api/Trace_UWBTag_MHEMapping/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_UWBTag_MHEMapping(int id)
        {
            try
            {
              // var ifexist = _repoWrapper.IUWBTagMHEMapping.GetTrace_UWBTag_MHEMapping(id);
                //if (ifexist == null)
                //{

                //    return NotFound();
                //}

               // _repoWrapper.IUWBTagMHEMapping.DeleteTrace_UWBTag_MHEMapping(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

    }
}
