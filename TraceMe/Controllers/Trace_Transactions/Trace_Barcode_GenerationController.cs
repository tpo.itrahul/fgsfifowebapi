﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.TraceTransactions;
using Model.Trace_Transactions;
using Repository;
namespace TraceMe.Controllers.Trace_Transactions
{
    [Route("api/[controller]")]
    [ApiController]
    public class Trace_Barcode_GenerationController : ControllerBase
    {
      
        private IRepositoryWrapper _repoWrapper;
        public Trace_Barcode_GenerationController( IRepositoryWrapper repowrapper)
        {
            _repoWrapper = repowrapper;
          
        }

        // GET: api/Trace_Barcode_Generation
        [HttpGet]
        public async Task<IActionResult> GetTrace_Barcode_Generation()
        {
            try
            {
                var result = await _repoWrapper.IBarcodeGeneration.GetTrace_Barcode_Generation();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET: api/Trace_Barcode_Generation/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrace_Barcode_Generation(int id)
        {

            try
            {
                var trace_Barcode_Generation = await _repoWrapper.IBarcodeGeneration.GetTrace_Barcode_Generation((id));
                if (trace_Barcode_Generation == null)
                {
                    return NotFound();
                }
                return Ok(trace_Barcode_Generation);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

          
        }

        // PUT: api/Trace_Barcode_Generation/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrace_Barcode_Generation(int id, TraceBarcodeGenerationDTO trace_Barcode_Generation)
        {
            if (id != trace_Barcode_Generation.Barcode_Id)
            {
                return BadRequest();
            }

         
            try
            {
                if (trace_Barcode_Generation == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.IBarcodeGeneration.GetTrace_Barcode_Generation(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IBarcodeGeneration.UpdateTrace_Barcode_Generation(trace_Barcode_Generation);
                await _repoWrapper.SaveAsync();
                return Ok(trace_Barcode_Generation);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


        }

        // POST: api/Trace_Barcode_Generation
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<IActionResult> PostTrace_Barcode_Generation(TraceBarcodeGenerationDTO trace_Barcode_Generation)
        {
            try
            {
                if (trace_Barcode_Generation == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.IBarcodeGeneration.CreateTrace_Barcode_Generation(trace_Barcode_Generation);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetTrace_Barcode_Generation", new { id = trace_Barcode_Generation.Barcode_Id }, trace_Barcode_Generation);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }

        // DELETE: api/Trace_Barcode_Generation/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_Barcode_Generation(int id)
        {
            try
            {
                var ifexist = _repoWrapper.IBarcodeGeneration.GetTrace_Barcode_Generation(id);
                if (ifexist == null)
                {

                    return NotFound();
                }

                _repoWrapper.IBarcodeGeneration.DeleteTrace_Barcode_Generation(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

    }
}
