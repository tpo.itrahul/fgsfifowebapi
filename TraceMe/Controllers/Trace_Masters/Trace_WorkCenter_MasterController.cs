﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.TraceMasters;
using Model.Trace_Masters;
using Repository;
namespace TraceMe.Controllers.Trace_Masters
{
    [Route("api/[controller]")]
    [ApiController]
    public class Trace_WorkCenter_MasterController : ControllerBase
    {
       
        private IRepositoryWrapper _repoWrapper;
        public Trace_WorkCenter_MasterController( IRepositoryWrapper repowrapper)
        {
            _repoWrapper = repowrapper;
           
        }

        // GET: api/Trace_WorkCenter_Master
        [HttpGet]
        public async Task<IActionResult> GetTrace_WorkCenter_Master()
        {
            try
            {
                var result = await _repoWrapper.IWorkCenterMaster.GetTrace_WorkCenter_Master();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
          
        }

        // GET: api/Trace_WorkCenter_Master/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrace_WorkCenter_Master(int id)
        {
            try
            {
                var trace_WorkCenter_Master = await _repoWrapper.IWorkCenterMaster.GetTrace_WorkCenter_Master((id));
                if (trace_WorkCenter_Master == null)
                {
                    return NotFound();
                }
                return Ok(trace_WorkCenter_Master);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

         
        }

        // PUT: api/Trace_WorkCenter_Master/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrace_WorkCenter_Master(int id, TraceWorkCenterMasterDTO trace_WorkCenter_Master)
        {
            if (id != trace_WorkCenter_Master.WorkCent_Id)
            {
                return BadRequest();
            }


            try
            {
                if (trace_WorkCenter_Master == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.IWorkCenterMaster.GetTrace_WorkCenter_Master(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IWorkCenterMaster.UpdateTrace_WorkCenter_Master(trace_WorkCenter_Master);
                await _repoWrapper.SaveAsync();
                return Ok(trace_WorkCenter_Master);


            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


        }

        // POST: api/Trace_WorkCenter_Master
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<IActionResult> PostTrace_WorkCenter_Master(TraceWorkCenterMasterDTO trace_WorkCenter_Master)
        {
            try
            {
                if (trace_WorkCenter_Master == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.IWorkCenterMaster.CreateTrace_WorkCenter_Master(trace_WorkCenter_Master);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetTrace_WorkCenter_Master", new { id = trace_WorkCenter_Master.WorkCent_Id }, trace_WorkCenter_Master);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


                     
        }

        // DELETE: api/Trace_WorkCenter_Master/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_WorkCenter_Master(int id)
        {
            try
            {
                var ifexist = _repoWrapper.IWorkCenterMaster.GetTrace_WorkCenter_Master(id);
                if (ifexist == null)
                {

                    return NotFound();
                }

                _repoWrapper.IWorkCenterMaster.DeleteTrace_WorkCenter_Master(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

    }
}
