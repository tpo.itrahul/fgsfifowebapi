﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.TraceMasters;
using Model.Trace_Masters;
using Repository;
namespace TraceMe.Controllers.Trace_Masters
{
    [Route("api/[controller]")]
    [ApiController]
    public class Trace_User_TypeController : ControllerBase
    {
     
        private IRepositoryWrapper _repoWrapper;
        public Trace_User_TypeController( IRepositoryWrapper repowrapper)
        {
            _repoWrapper = repowrapper;
          
        }

        // GET: api/Trace_User_Type
        [HttpGet]
        public async Task<IActionResult> GetTrace_User_Type()
        {
            try
            {
                var result = await _repoWrapper.IUserType.GetTrace_User_Type();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET: api/Trace_User_Type/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrace_User_Type(int id)
        {
            try
            {
                var trace_User_Type = await _repoWrapper.IUserType.GetTrace_User_Type((id));
                if (trace_User_Type == null)
                {
                    return NotFound();
                }
                return Ok(trace_User_Type);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


        }

        // PUT: api/Trace_User_Type/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrace_User_Type(int id, TraceUserTypeDTO trace_User_Type)
        {
            if (id != trace_User_Type.User_Type_Id)
            {
                return BadRequest();
            }
                     

            try
            {
                if (trace_User_Type == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.IUserType.GetTrace_User_Type(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IUserType.UpdateTrace_User_Type(trace_User_Type);
                await _repoWrapper.SaveAsync();
                return Ok(trace_User_Type);


            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }

        // POST: api/Trace_User_Type
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<IActionResult> PostTrace_User_Type([FromBody] TraceUserTypeDTO trace_User_Type)
        {
            try
            {
                if (trace_User_Type == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.IUserType.CreateTrace_User_Type(trace_User_Type);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetTrace_User_Type", new { id = trace_User_Type.User_Type_Id }, trace_User_Type);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
           
        }

        // DELETE: api/Trace_User_Type/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_User_Type(int id)
        {
            try
            {
                var ifexist = _repoWrapper.IUserType.GetTrace_User_Type(id);
                if (ifexist == null)
                {

                    return NotFound();
                }

                _repoWrapper.IUserType.DeleteTrace_User_Type(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

    }
}
