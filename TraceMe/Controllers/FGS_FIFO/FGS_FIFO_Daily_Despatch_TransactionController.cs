﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.FGSFIFO;
using Model.FGS_FIFO;
using Repository;


namespace TraceMe
{
    [Route("api/[controller]")]
    [ApiController]
    public class FGS_FIFO_Daily_Despatch_TransactionController : ControllerBase
    {

        private IRepositoryWrapper _repoWrapper;

        public FGS_FIFO_Daily_Despatch_TransactionController(IRepositoryWrapper repowrapper)
        {

            _repoWrapper = repowrapper;
        }



        [HttpGet]
        public async Task<IActionResult> GetFGS_Daily_Despatch_Transaction()
        {
            try
            {
                var result = await _repoWrapper.IDaily_DespatchTranscation.GetFGS_Daily_Despatch_Transaction();
                
                //_logger.LogInfo($"Returned all owners from database.");
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, ex);
            }

        }



        [HttpGet("{id}")]
        public async Task<IActionResult> GetDaily_DespatchTranscation(int id)
        {
            try
            {
                var fGS_FIFO_Daily_DespatchTranscation = await _repoWrapper.IDaily_DespatchTranscation.GetFGS_Daily_Despatch_Transaction(id);

                if (fGS_FIFO_Daily_DespatchTranscation == null)
                {
                    return NotFound();
                }

                return Ok(fGS_FIFO_Daily_DespatchTranscation);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpGet("FetchMaterialFor{custid,materialTypeId}")]
        public async Task<IActionResult> GetDaily_DespatchTranscationMaterialID(int custid,int id)
        {
            try
            {
    //            var student = _context.Students
    //.FromSqlRaw(@"SELECT * FROM Student WHERE Name = {0}", "John Doe")
    //.FirstOrDefault();

                var fGS_FIFO_Daily_DespatchTranscation = await _repoWrapper.IDaily_DespatchTranscation.GetFGS_Daily_Despatch_Transaction(id);

                if (fGS_FIFO_Daily_DespatchTranscation == null)
                {
                    return NotFound();
                }

                return Ok(fGS_FIFO_Daily_DespatchTranscation);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpPut("{id}")]
        public async Task<IActionResult> PutFGS_FIFO_DespatchTranscation(int id, DailyDespatchTransactionDTO fGS_FIFO_Daily_DespatchTranscation)
        {
            try
            {
                if (fGS_FIFO_Daily_DespatchTranscation == null)

                {
                    return BadRequest("Object is null");
                }

                var ifexist = _repoWrapper.IDaily_DespatchTranscation.GetFGS_Daily_Despatch_Transaction(id);
                if (ifexist == null)
                {

                    return NotFound();
                }


                _repoWrapper.IDaily_DespatchTranscation.UpdateFGS_Daily_Despatch_Transaction(fGS_FIFO_Daily_DespatchTranscation);
                await _repoWrapper.SaveAsync();
                return Ok(fGS_FIFO_Daily_DespatchTranscation);
            }
            catch (DbUpdateConcurrencyException)
            {
                return StatusCode(500, "Internal server error");

            }

        }

        [HttpPost]
        public async Task<IActionResult> PostDaily_DespatchTranscation([FromBody] DailyDespatchTransactionDTO model)
         {
            try
            {
                if (model == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest("Owner object is null");
                }
                //  int availablestock = 0;

                int RequiredQuantity = 0;
                int totdespatched =0;
                int QuantityPending = 0;




                 RequiredQuantity = _repoWrapper.IDespatch_Request.TotQuantity_Despatch_Request_Placed_For_RID(model.Desp_Req_Id);
                 totdespatched = _repoWrapper.IDaily_DespatchTranscation.Get_tot_despatchedForRequestId(model.Desp_Req_Id);
                 QuantityPending = RequiredQuantity - totdespatched;
                if(QuantityPending >= model.Quantity_Despatched)
                {
                    //chk scanned barcode contains required amount
                   
                    int stockinBarcode =  _repoWrapper.IDaily_ReceiptTranscation.Get_Supplied_Stock_Barcode(model.Barcode);
                    int consumedstockBC = _repoWrapper.IDaily_DespatchTranscation.Get_tot_DespatchedForScannedBarcode(model.Barcode);
                    int AvailableStockInBarcode = stockinBarcode - consumedstockBC;
                    if(AvailableStockInBarcode >= model.Quantity_Despatched)
                    {
                        int tid = _repoWrapper.IDaily_DespatchTranscation.CreateFGS_Daily_Despatch_Transaction(model);
                        await _repoWrapper.SaveAsync();
                        //Update transaction status in both tables after adding the transaction in Dailydespacth transaction
                         //RequiredQuantity = _repoWrapper.IDespatch_Request.TotQuantity_Despatch_Request_Placed_For_RID(model.Desp_Req_Id);
                         totdespatched = _repoWrapper.IDaily_DespatchTranscation.Get_tot_despatchedForRequestId(model.Desp_Req_Id);
                         QuantityPending = RequiredQuantity - totdespatched;
                        if (QuantityPending == 0)
                        {
                            _repoWrapper.IDaily_DespatchTranscation.ChangeTranscationProcessStatus(tid, 4);
                            _repoWrapper.IDespatch_Request.ChangeTranscationStatus(model.Desp_Req_Id, 0);
                            await _repoWrapper.SaveAsync();
                        }
                        else if( RequiredQuantity > QuantityPending)
                        {
                            _repoWrapper.IDaily_DespatchTranscation.ChangeTranscationProcessStatus(tid, 2);
                        }

                          
                    }
                    
                    else
                    {
                        return this.Ok("Entered quantity not available here");
                    }
                      
                    //update status as partially despatched after inserting details to table
                   
              // _repoWrapper.IDaily_DespatchTranscation.UpdateFGS_Daily_Despatch_Transaction(model);
                  //  await _repoWrapper.SaveAsync();

                   // return this.Ok(model);
                    return CreatedAtAction("GetFGS_Daily_Despatch_Transaction", new { id = model.Desp_TransId }, model);
                }
               
                else
                {
                    return StatusCode(500, "Internal server error");
                }

               
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }




        [HttpDelete("")]
        public async Task<IActionResult> DeleteFGS_FIFO_DespatchTranscation(int id)
        {
            try
            {
                //var ifexist = _repoWrapper.IDaily_DespatchTranscation.GetFGS_Daily_Despatch_Transaction(id);
                //if (ifexist == null)
                //{

                //    return NotFound();
                //}


                _repoWrapper.IDaily_DespatchTranscation.DeleteFGS_Daily_Despatch_Transaction(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


        }



    }
}
