﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.FGSFIFO;
using Model.FGS_FIFO;
using Repository;

namespace TraceMe
{
    [Route("api/[controller]")]
    [ApiController]
    public class FGS_FIFO_CustomerType_MasterController : ControllerBase
    {

        private IRepositoryWrapper _repoWrapper;

        public FGS_FIFO_CustomerType_MasterController(IRepositoryWrapper repowrapper)
        {

            _repoWrapper = repowrapper;
        }

        // GET: api/FGS_FIFO_BatchType_Master
        //Modified HttpGet
        [HttpGet]
        public async Task<IActionResult> GetFGS_FIFO_CustomerTypeMaster()
        {
            try
            {
                var result = await _repoWrapper.ICustomer_Type.GetFGS_CustomerType_Master();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET: api/FGS_FIFO_BatchType_Master/5


        [HttpGet("{id}")]
        public async Task<IActionResult> GetFGS_CustomerType_Master(int id)
        {

            try
            {
                var fGS_FIFO_CustomerType_Master = await _repoWrapper.ICustomer_Type.GetFGS_CustomerType_Master((id));
                if (fGS_FIFO_CustomerType_Master == null)
                {
                    return NotFound();
                }
                return Ok(fGS_FIFO_CustomerType_Master);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }



        }

        // PUT: api/FGS_FIFO_BatchType_Master/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFGS_FIFO_BatchType_Master(int id, CustomerTypeMasterDTO FGS_FIFO_CustomerType_Master)
        {
            if (id != FGS_FIFO_CustomerType_Master.Customer_Type_Id)
            {
                return BadRequest();
            }


            try
            {
                if (FGS_FIFO_CustomerType_Master == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.ICustomer_Type.GetFGS_CustomerType_Master(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.ICustomer_Type.UpdateFGS_CustomerType_Master(FGS_FIFO_CustomerType_Master);
                await _repoWrapper.SaveAsync();
                return Ok(FGS_FIFO_CustomerType_Master);


            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

            // return NoContent();
        }
    

        // POST: api/FGS_FIFO_BatchType_Master
        [HttpPost]
        public async Task<IActionResult>  PostFGS_FIFO_CustomerType_Master([FromBody] CustomerTypeMasterDTO model)
        {
            try
            {
                if (model == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.ICustomer_Type.CreateFGS_CustomerType_Master(model);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetFGS_CustomerType_Master", new { id = model.Customer_Type_Id }, model);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFGS_FIFO_CustomerType_Master(int id)
        {


            try
            {
                //var ifexist = _repoWrapper.ICustomer_Type.GetFGS_CustomerType_Master(id);
                //if (ifexist == null)
                //{

                //    return NotFound();
                //}

                _repoWrapper.ICustomer_Type.DeleteFGS_CustomerType_Master(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }



    }
}
