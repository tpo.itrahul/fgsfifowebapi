﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.FGS_FIFO;
using Model.FGS_FIFO;
using Repository;
using Entity.FGSFIFO;
namespace TraceMe
{
    [Route("api/[controller]")]
    [ApiController]
    public class FGS_FIFO_Cust_Ageing_MappingController : ControllerBase
    {
        private IRepositoryWrapper _repoWrapper;

        public FGS_FIFO_Cust_Ageing_MappingController( IRepositoryWrapper repowrapper)
        {
          
            _repoWrapper = repowrapper;
        }

        // GET: api/FGS_FIFO_Cust_Ageing_Mapping
        //Modified HttpGet
        [HttpGet]
        public async Task<IActionResult> GetFGS_FIFO_Cust_Ageing_Mapping()
        {
            try
            {
                var result = await _repoWrapper.ICustomerAgeing_Mapping.GetFGS_Cust_Ageing_Mapping();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
          
        }

        // GET: api/FGS_FIFO_Material_Master/5
  

        [HttpGet("{id}")]
        public async Task<IActionResult> GetFGS_FIFO_Cust_Ageing_Mapping(int id)
        {

            try
            {
                var fGS_FIFO_CustAgeing_Mapping =await _repoWrapper.ICustomerAgeing_Mapping.GetFGS_Cust_Ageing_Mapping(id);

                if (fGS_FIFO_CustAgeing_Mapping == null)
                {
                    return NotFound();
                }

                return Ok(fGS_FIFO_CustAgeing_Mapping);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
         
        }

       
        [HttpPut("{id}")]
        public async Task<IActionResult> GetFGS_FIFO_Cust_Ageing_Mapping(int id, CustomerAgeingMappingDTO fGS_FIFO_CustAgeing_Mapping)
        {
            if (id != fGS_FIFO_CustAgeing_Mapping.Cust_Ageing_Id)
            {
                return BadRequest();
            }

        
            try
            {
                if (fGS_FIFO_CustAgeing_Mapping == null)
                    
                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.ICustomerAgeing_Mapping.GetFGS_Cust_Ageing_Mapping(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.ICustomerAgeing_Mapping.UpdateFGS_Cust_Ageing_Mapping(fGS_FIFO_CustAgeing_Mapping);
               await _repoWrapper.SaveAsync();
                return Ok(fGS_FIFO_CustAgeing_Mapping);
            }
            catch (DbUpdateConcurrencyException)
            {
                return StatusCode(500, "Internal server error");
            }

           // return NoContent();
        }

        // POST: api/FGS_FIFO_Ageing_Master
        [HttpPost]
        public async Task<IActionResult>  PostFGS_FIFO_Cust_Ageing_Mapping([FromBody] CustomerAgeingMappingDTO model)
        {

            try
            {
                if (model == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest("Owner object is null");
                }
                _repoWrapper.ICustomerAgeing_Mapping.CreateFGS_Cust_Ageing_Mapping(model);
                await _repoWrapper.SaveAsync();
                return this.Ok(model);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
            
          
        }

        // DELETE: api/FGS_FIFO_Ageing_Master/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFGS_FIFO_CustAgeing_Mapping(int id)
        {
            try
            {
                //var ifexist = _repoWrapper.ICustomerAgeing_Mapping.GetFGS_Cust_Ageing_Mapping(id);
                //if (ifexist == null)
                //{

                //    return NotFound();
                //}

                _repoWrapper.IAgeing_Master.DeleteFGS_FGS_Ageing(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

    }
}
