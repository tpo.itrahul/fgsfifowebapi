﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Tread_FIFO
{
    [Table("Trace_Status_Master")]
    public  class Trace_Status_Master
    {
     


        [Key]
        public int Status_Id { get; set; }
        [MaxLength(30)]
        public string? Status_Code { get; set; }
        [MaxLength(50)]
        public string? Status_Name { get; set; }
     
    }
}
