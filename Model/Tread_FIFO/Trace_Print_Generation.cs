﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Trace_Transactions;
namespace Model.Tread_FIFO
{
    [Table("Trace_Print_Generation")]
    public class Trace_Print_Generation
    {
 
        [Key]
        public int Print_Id { get; set; }
        [ForeignKey(nameof(Trace_Production))]
        [Required(ErrorMessage = "Production_Id is required")]
        public int Production_Id { get; set; }
        public DateTime? Print_Time { get; set; }
        [MaxLength(50)]
        public string? Printed_By { get; set; }
        public int? Barcode_Id { get; set; }
        public int? Status { get; set; }
    }


}

