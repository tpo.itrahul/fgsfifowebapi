﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Trace_Masters;

namespace Model.Tread_FIFO
{
    [Table("Trace_Scrap_Entry")]
    public class Trace_Scrap_Entry
    {
        [Key]
        public int Scrap_EntryId { get; set; }
        [ForeignKey(nameof(Trace_Material_Master))]
        [Required(ErrorMessage = "Material_Id is required")]
        public int Material_Id { get; set; }
        [MaxLength(50)]
        public string? Updated_By { get; set; }
        public DateTime? Entry_Date { get; set; }
        public DateTime? Entry_Time { get; set; }


    }
}
