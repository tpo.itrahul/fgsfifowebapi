﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Model.Tread_FIFO
{
    [Table("Trace_Hold_Reasons")]
    public class Trace_Hold_Reasons
    {
    
        [Key]
        public int Hold_Reason_Id { get; set; }

        [Required(ErrorMessage = "Hold_Reason is required")]
        [MaxLength(50)]
        public string Hold_Reason { get; set; }

        public int? Hold_Reason_status { get; set; }

    }
}
