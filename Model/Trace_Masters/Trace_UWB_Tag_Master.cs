﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Model.Trace_Masters
{
    [Table("Trace_UWB_Tag_Master")]
    public class Trace_UWB_Tag_Master
    {


        [Key]
        public int UWB_Tag_Id { get; set; }

        public string? UWB_Tag_Name { get; set; }
        public int? UWB_Tag_Uniqe_Id { get; set; }
        public int? UWB_Tag_Status { get; set; }
      
    }
}
