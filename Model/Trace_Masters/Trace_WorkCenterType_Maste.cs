﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Trace_Masters
{
    [Table("Trace_WorkCenterType_Master")]
    public class Trace_WorkCenterType_Master
    {


        [Key]
        public int WorkCentType_Id { get; set; }

        public string? WorkCentType_Name { get; set; }
        public int? Status { get; set; }
       
    }
}
