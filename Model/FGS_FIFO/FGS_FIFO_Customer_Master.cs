﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.Extensions.Logging;
namespace Model.FGS_FIFO
{
    [Table("FGS_FIFO_Customer_Master")]
    public class FGS_FIFO_Customer_Master

    {
        [Key]
        public int Customer_Id { get; set; }
   
        [Required(ErrorMessage = "Customer_Name is required")]
        [StringLength(150, ErrorMessage = "Customer_Name cannot be longer than 150 characters")]
        public string Customer_Name { get; set; }

        [Required(ErrorMessage = "Customer_Address is required")]
        [StringLength(150, ErrorMessage = "Customer_Address cannot be longer than 150 characters")]
        public string Customer_Address { get; set; }

        [ForeignKey(nameof(FKFGS_FIFO_Batch_Type_Master))]
        [Required(ErrorMessage = "Batch_Type_Id is required")]
       
        public int? Batch_Type_Id { get; set; }


        [ForeignKey(nameof(FGS_FIFO_Customer_Type_Master))]
        [Required(ErrorMessage = "Customer_Type_Id is required")]
        public int Customer_Type_Id { get; set; }
        public FGS_FIFO_Customer_Type_Master FGS_FIFO_Customer_Type_Master { get; set; }
        public FGS_FIFO_Batch_Type_Master FKFGS_FIFO_Batch_Type_Master { get; set; }
        public int? Cust_Status { get; set; }
    }
}




