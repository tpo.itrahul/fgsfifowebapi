﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.TraceUsers
{
    [Table("Trace_Application_Employee_Mapping")]
    public  class Trace_Application_Employee_Mapping
    {
            

        [Key]
        public int Id { get; set; }

        [ForeignKey(nameof(Trace_Module_Master))]
        [Required(ErrorMessage = "Module_Id is required")]
        public int Module_Id { get; set; }


        [ForeignKey(nameof(Trace_Login_Master))]
        [Required(ErrorMessage = "LogMastEmp_Id is required")]
        public int LogMastEmp_Id { get; set; }
        public int View { get; set; }
        public int Update { get; set; }
        public int Delete { get; set; }
        public int Add { get; set; }

        public Trace_Login_Master Trace_Login_Master { get; set; }

        public Trace_Module_Master Trace_Module_Master { get; set; }

    }
}
