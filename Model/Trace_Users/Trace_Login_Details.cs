﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.TraceUsers
{
    [Table("Trace_Login_Details")]
    public    class Trace_Login_Details
    {
       

        [Key]
        public int Id { get; set; }
        public int Emp_Id { get; set; }
        public int Module_Id { get; set; }
        public int Logged_Time { get; set; }
        public int Plant_Id { get; set; }

    }
}
