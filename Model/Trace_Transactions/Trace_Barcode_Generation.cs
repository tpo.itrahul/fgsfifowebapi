﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Trace_Transactions
{

    [Table("Trace_Barcode_Generation")]
    public class Trace_Barcode_Generation
    {
        [Key]
        public int Barcode_Id { get; set; }
        public DateTime? Barcode_Gen_Date { get; set; }
        public DateTime? Barcode_Gen_Time { get; set; }

        [ForeignKey(nameof(Trace_Transactions.Trace_Production))]
        [Required(ErrorMessage = "Production_Id is required")]
        public int Production_Id { get; set; }

        [Required(ErrorMessage = "Barcode is required")]
        [MaxLength(50)]
        public string? Barcode { get; set; }
        public int? BarcodeGen_Status { get; set; }


    }
}
