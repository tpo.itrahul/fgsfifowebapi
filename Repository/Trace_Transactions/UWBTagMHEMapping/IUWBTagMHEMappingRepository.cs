﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Trace_Transactions;
using Entity.TraceTransactions;
namespace Repository.Trace_Transactions
{
  public  interface IUWBTagMHEMappingRepository 
    {

        Task<IEnumerable<TraceUWBTagMHEMappingDTO>> GetTrace_TraceUWBTAgMHEMapping();

        Task<IEnumerable<TraceUWBTagMHEMappingDTO>> GetTrace_TraceUWBTAgMHEMapping(int id);

        void CreateTrace_TraceUWBTAgMHEMapping(TraceUWBTagMHEMappingDTO model);

        void UpdateTrace_TraceUWBTAgMHEMapping(TraceUWBTagMHEMappingDTO model);

        void DeleteTrace_TraceUWBTAgMHEMapping(int id);
       

    }
}
