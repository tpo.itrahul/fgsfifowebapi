﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Trace_Transactions;
using Entity.TraceTransactions;
namespace Repository.Trace_Transactions
{
    public interface ITagGenerationRepository //: IRepositoryBase<Trace_Barcode_Generation>
    {
         Task<IEnumerable<TraceTagGenerationDTO>> GetTrace_Tag_Generation();

        Task<IEnumerable<TraceTagGenerationDTO>> GetTrace_Tag_Generation(int id);

       void CreateTrace_Tag_Generation(TraceTagGenerationDTO model);

       void UpdateTrace_Tag_Generation(TraceTagGenerationDTO model);

       void DeleteTrace_Tag_Generation(int id);
       


    }
}
