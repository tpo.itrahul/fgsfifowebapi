﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Trace_Transactions;
using Entity.TraceTransactions ;
namespace Repository.Trace_Transactions
{
    public interface IMaterialConsumedSpecRepository //: IRepositoryBase<Trace_Material_Consumed_Spec>
    {
        Task<IEnumerable<TraceMaterialConsumedSpecDTO>> GetTrace_Material_Consumed_Spec();

        Task<IEnumerable<TraceMaterialConsumedSpecDTO>> GetTrace_Material_Consumed_Spec(int id);

        void CreateTrace_Material_Consumed_Spec(TraceMaterialConsumedSpecDTO model);

        void UpdateTrace_Material_Consumed_Spec(TraceMaterialConsumedSpecDTO model);

        void DeleteTrace_Material_Consumed_Spec(int id);
       
    }
}
