﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Trace_Transactions;
using Entity.FGS_FIFO;
using Entity.TraceTransactions;
using Microsoft.EntityFrameworkCore;
namespace Repository.Trace_Transactions
{
    public class BarcodeGenerationRepository : RepositoryBase<Trace_Barcode_Generation>, IBarcodeGenerationRepository
    {
        public BarcodeGenerationRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TraceBarcodeGenerationDTO>> GetTrace_Barcode_Generation()
        {

            var rows = FindAll();
            var results = rows.Select(x => new TraceBarcodeGenerationDTO()
            {
                Barcode_Id = x.Barcode_Id,
                Barcode = x.Barcode,
                Barcode_Gen_Date = x.Barcode_Gen_Date,
                Barcode_Gen_Time = x.Barcode_Gen_Time,
                BarcodeGen_Status = x.BarcodeGen_Status,
                Production_Id = x.Production_Id
            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<TraceBarcodeGenerationDTO>> GetTrace_Barcode_Generation(int id)
        {


            var rows = FindByCondition(x => x.Barcode_Id.Equals(id));
            var results = rows.Select(x => new TraceBarcodeGenerationDTO()
            {
                Barcode_Id = x.Barcode_Id,
                Barcode = x.Barcode,
                Barcode_Gen_Date = x.Barcode_Gen_Date,
                Barcode_Gen_Time = x.Barcode_Gen_Time,
                BarcodeGen_Status = x.BarcodeGen_Status,
                Production_Id = x.Production_Id
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_Barcode_Generation(TraceBarcodeGenerationDTO model)
        {
            var entity = new Trace_Barcode_Generation();
            entity.Barcode_Id = model.Barcode_Id;
            entity.Barcode = model.Barcode;
            entity.Barcode_Gen_Date = model.Barcode_Gen_Date;
            entity.Barcode_Gen_Time = model.Barcode_Gen_Time;
            entity.BarcodeGen_Status = model.BarcodeGen_Status;
            entity.Production_Id = model.Production_Id;

            Create(entity);
        }
        public void UpdateTrace_Barcode_Generation(TraceBarcodeGenerationDTO model)
        {
            var entity = new Trace_Barcode_Generation();
            if (model != null)
            {
                if (model.Barcode_Id > 0)
                {
                    var data = FindByCondition(x => x.Barcode_Id == model.Barcode_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.Barcode_Id = model.Barcode_Id;
                entity.Barcode = model.Barcode;
                entity.Barcode_Gen_Date = model.Barcode_Gen_Date;
                entity.Barcode_Gen_Time = model.Barcode_Gen_Time;
                entity.BarcodeGen_Status = model.BarcodeGen_Status;
                entity.Production_Id = model.Production_Id;
            }
            Update(entity);
        }
        public void DeleteTrace_Barcode_Generation(int id)
        {
            var entity = new Trace_Barcode_Generation();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Barcode_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                entity.Barcode_Id = 0;
            }

            Update(entity);
        }
    }
}
