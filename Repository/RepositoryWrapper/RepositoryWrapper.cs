﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repository.FGS_FIFO;
using Repository.Tread_FIFO;
using Repository.Trace_Masters;
using Repository.Trace_Transactions;
using Repository.Trace_Users;
using Entity.FGS_FIFO;



namespace Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private readonly Trace_DBContext _repoContext;
        private IFGSMaterialMasterRepository _fgs_material_master;
        private IFGSAgeingMasterRepository _fgs_ageing_master;
        private IFGSBatchTypeMasterRepository _fgs_batchtype_master;
        private IFGSCustomerMasterRepository _fgs_customer_master;
        private IFGSCustAgeingMappingRepository _fgs_ageingcustomer_mapping;
        private IFGSCustomerTypeMasterRepository _fgs_customer_type;
        private IFGSDailyDespatchTransactionRepository _fgs_daily_despatchtranscation;
        private IFGSLocationMasterRepository _fgs_Location_Master;
        private IFGSMaterialTypeMasterRepository _fgs_MaterailType_Master;
        private IFGSSupplierMasterRepository _fgs_Supplier_Master;
        private IFGSDespatchStatusMasterRepository _fgs_DespatchStatus_Master;
        private IFGSFGSDespatchRequestRepository _fgs_Despatch_Request;
        private IFGSDailyReceiptTransactionRepository _fgs_Daily_ReceiptTransaction;

        //Tread FIFO
        private IHoldRepository _tread_hold;
        private IBarcodeGenerationRepository _tread_barcode_generation;
        private IHoldReasonsRepository _tread_hold_reason;
        private IHoldReleaseRepository _tread_hold_release;
        private IMaterialTransactionRepository _tread_material_transaction;
        private IPrintGenerationRepository _tread_print_generation;
        private IProductAgeingRepository _tread_product_ageing;
        private IProductionRemarksRepository _tread_production_remarks;
        private IScrapEntryRepository _tread_scrap_entry;
        private IStatusMasterRepository _tread_status_master;
        private ITakenToTBRepository _tread_taken_to_tb;

        //transcation
        private IMaterialConsumedActualRepository _trace_materialconsumed_actual;
        private IMaterialConsumedSpecRepository _trace_materialconsumed_spec;
        private ITraceProductionRepository _trace_production;
        private ITagGenerationRepository _trace_tag_generation;
        private IUWBTagMHEMappingRepository _trace_UWBTag_MHEMapping;
        //masters
        private IMaterialMasterRepository _trace_materailmaster;
        private IMHEMasterRepository _trace_MHE_master;
        private IPlantMasterRepository _trace_plant_master;
        private IStorageLocationMasterRepository _trace_storage_location;
        private IUserTypeRepository _trace_user_type;
        private IUWBAnchorMasterRepository _trace_uwb_anchormaster;
        private IUWBTagMasterRepository _trace_uwbtag_master;
        private IWorkCenterMasterRepository _trace_workcenter_master;

        //users

        private IModuleMasterRespository _trace_module_master;
        private IApplicationEmployeeMappingRepository _trace_applicationemployee_mapping;
        private IApplicationMasterRepository _trace_application_master;
        private ILoginDetailsRepository _trace_login_details;
        private ILoginMasterRepository _trace_Login_master;

        public RepositoryWrapper(Trace_DBContext repositoryContext)
        {
            _repoContext = repositoryContext;
        }
        public IFGSMaterialMasterRepository IMaterial_Master
        {
            get
            {
                if (_fgs_material_master == null)
                {
                    _fgs_material_master = new FGSMaterialMasterRepository(_repoContext);
                }
                return _fgs_material_master;
            }
        }
        public IFGSAgeingMasterRepository IAgeing_Master
        {
            get
            {
                if (_fgs_ageing_master == null)
                {
                    _fgs_ageing_master = new FGSAgeingMasterRepository(_repoContext);
                }
                return _fgs_ageing_master;
            }
        }
        public IFGSBatchTypeMasterRepository IBatchType_Master
        {
            get
            {
                if (_fgs_batchtype_master == null)
                {
                    _fgs_batchtype_master = new FGSBatchTypeMasterRepository(_repoContext);
                }
                return _fgs_batchtype_master;
            }
        }

        public IFGSCustomerMasterRepository ICustomer_Master
        {
            get
            {
                if (_fgs_customer_master == null)
                {
                    _fgs_customer_master = new FGSCustomerMasterRepository(_repoContext);
                }
                return _fgs_customer_master;
            }
        }
        public IFGSCustAgeingMappingRepository ICustomerAgeing_Mapping
        {
            get
            {
                if (_fgs_ageingcustomer_mapping == null)
                {
                    _fgs_ageingcustomer_mapping = new FGSCustAgeingMappingRepository(_repoContext);
                }
                return _fgs_ageingcustomer_mapping;
            }
        } 
        public IFGSCustomerTypeMasterRepository ICustomer_Type
        {
            get
            {
                if (_fgs_customer_type == null)
                {
                    _fgs_customer_type = new FGSCustomerTypeMasterRepository(_repoContext);
                }
                return _fgs_customer_type;
            }
        }
        public IFGSDailyDespatchTransactionRepository IDaily_DespatchTranscation
        {
            get
            {
                if (_fgs_daily_despatchtranscation == null)
                {
                    _fgs_daily_despatchtranscation = new FGSDailyDespatchTransactionRepository(_repoContext);
                }
                return _fgs_daily_despatchtranscation;
            }
        }
        public IFGSLocationMasterRepository ILocation_Master
        {
            get
            {
                if (_fgs_Location_Master == null)
                {
                    _fgs_Location_Master = new FGSLocationMasterRepository(_repoContext);
                }
                return _fgs_Location_Master;
            }
        }
        public IFGSMaterialTypeMasterRepository IMaterialType_Master
        {
            get
            {
                if (_fgs_MaterailType_Master == null)
                {
                    _fgs_MaterailType_Master = new FGSMaterialTypeMasterRepository(_repoContext);
                }
                return _fgs_MaterailType_Master;
            }
        }
        public IFGSSupplierMasterRepository ISupplier_Master
        {
            get
            {
                if (_fgs_Supplier_Master == null)
                {
                    _fgs_Supplier_Master = new FGSSupplierMasterRepository(_repoContext);
                }
                return _fgs_Supplier_Master;
            }
        }
        public IFGSDespatchStatusMasterRepository IDespatchStatus_Master
        {
            get
            {
                if (_fgs_DespatchStatus_Master == null)
                {
                    _fgs_DespatchStatus_Master = new FGSDespatchStatusMasterRepository(_repoContext);
                }
                return _fgs_DespatchStatus_Master;
            }
        }
        public IFGSFGSDespatchRequestRepository IDespatch_Request
        {
            get
            {
                if (_fgs_Despatch_Request == null)
                {
                    _fgs_Despatch_Request = new FGSDespatchRequestRepository(_repoContext);
                }
                return _fgs_Despatch_Request;
            }
        }

        public IFGSDailyReceiptTransactionRepository IDaily_ReceiptTranscation
        { 
            get
            {
                if (_fgs_Daily_ReceiptTransaction == null)
                {
                    _fgs_Daily_ReceiptTransaction = new FGSDailyReceiptTransactionRepository(_repoContext);
                }
                return _fgs_Daily_ReceiptTransaction;
            }
        }

        public IHoldRepository IHold
        {
            get
            {
                if (_tread_hold == null)
                {
                    _tread_hold = new HoldRepository(_repoContext);
                }
                return _tread_hold;
            }
        }

        public IBarcodeGenerationRepository IBarcodeGeneration
        {
            get
            {
                if (_tread_barcode_generation == null)
                {
                    _tread_barcode_generation = new BarcodeGenerationRepository(_repoContext);
                }
                return _tread_barcode_generation;
            }
        }

        public ITagGenerationRepository ITagGeneration
        {
            get
            {
                if (_trace_tag_generation == null)
                {
                    _trace_tag_generation = new TagGenerationRepository(_repoContext);
                }
                return _trace_tag_generation;
            }
        }
        public IUWBTagMHEMappingRepository IUWBTagMHEMapping
        {
            get
            {
                if (_trace_UWBTag_MHEMapping == null)
                {
                    _trace_UWBTag_MHEMapping = new UWBTagMHEMappingRepository(_repoContext);
                }
                return _trace_UWBTag_MHEMapping;
            }
        }
        public IHoldReasonsRepository IHoldReasons
        {
            get
            {
                if (_tread_hold_reason == null)
                {
                    _tread_hold_reason = new HoldReasonsRepository(_repoContext);
                }
                return _tread_hold_reason;
            }
        }
        public IHoldReleaseRepository IHoldRelease
        {
            get
            {
                if (_tread_hold_release == null)
                {
                    _tread_hold_release = new HoldReleaseRepository(_repoContext);
                }
                return _tread_hold_release;
            }
        }
        public IMaterialTransactionRepository IMaterialTransaction
        {
            get
            {
                if (_tread_material_transaction == null)
                {
                    _tread_material_transaction = new MaterialTransactionRepository(_repoContext);
                }
                return _tread_material_transaction;
            }
        }

        public IPrintGenerationRepository IPrintGeneration
        {
            get
            {
                if (_tread_print_generation == null)
                {
                    _tread_print_generation = new PrintGenerationRepository(_repoContext);
                }
                return _tread_print_generation;
            }
        }

        public IProductAgeingRepository IProductAgeing
        {
            get
            {
                if (_tread_product_ageing == null)
                {
                    _tread_product_ageing = new ProductAgeingRepository(_repoContext);
                }
                return _tread_product_ageing;
            }
        }

        public IProductionRemarksRepository IProductionRemarks
        {
            get
            {
                if (_tread_production_remarks == null)
                {
                    _tread_production_remarks = new ProductionRemarksRepository(_repoContext);
                }
                return _tread_production_remarks;
            }
        }
        public IScrapEntryRepository IScrapEntry
        {
            get
            {
                if (_tread_scrap_entry == null)
                {
                    _tread_scrap_entry = new ScrapEntryRepository(_repoContext);
                }
                return _tread_scrap_entry;
            }
        }
        public IStatusMasterRepository IStatusMaster
        {
            get
            {
                if (_tread_status_master == null)
                {
                    _tread_status_master = new StatusMasterRepository(_repoContext);
                }
                return _tread_status_master;
            }
        }
        public ITakenToTBRepository ITakenToTB
        {
            get
            {
                if (_tread_taken_to_tb == null)
                {
                    _tread_taken_to_tb = new TakenToTBRepository(_repoContext);
                }
                return _tread_taken_to_tb;
            }
        }
        public IMaterialConsumedActualRepository IMaterialConsumedActual
        {
            get
            {
                if (_trace_materialconsumed_actual == null)
                {
                    _trace_materialconsumed_actual = new MaterialConsumedActualRepository(_repoContext);
                }
                return _trace_materialconsumed_actual;
            }
        }
        public IMaterialConsumedSpecRepository IMaterialConsumedSpec
        {
            get
            {
                if (_trace_materialconsumed_spec == null)
                {
                    _trace_materialconsumed_spec = new MaterialConsumedSpecRepository(_repoContext);
                }
                return _trace_materialconsumed_spec;
            }
        }
        public ITraceProductionRepository ITraceProduction
        {
            get
            {
                if (_trace_production == null)
                {
                    _trace_production = new TraceProductionRepository(_repoContext);
                }
                return _trace_production;
            }
        }
        public IMaterialMasterRepository IMaterialMaster
        {
            get
            {
                if (_trace_materailmaster == null)
                {
                    _trace_materailmaster = new MaterialMasterRepository(_repoContext);
                }
                return _trace_materailmaster;
            }
        }
        public IMHEMasterRepository IMHEMaster
        {
            get
            {
                if (_trace_MHE_master == null)
                {
                    _trace_MHE_master = new MHEMasterRepository(_repoContext);
                }
                return _trace_MHE_master;
            }
        }
        public IPlantMasterRepository IPlantMaster
        {
            get
            {
                if (_trace_plant_master == null)
                {
                    _trace_plant_master = new PlantMasterRepository(_repoContext);
                }
                return _trace_plant_master;
            }
        }
        public IStorageLocationMasterRepository IStorageLocationMaster
        {
            get
            {
                if (_trace_storage_location == null)
                {
                    _trace_storage_location = new StorageLocationMasterRepository(_repoContext);
                }
                return _trace_storage_location;
            }
        }
        public IUserTypeRepository IUserType
        {
            get
            {
                if (_trace_user_type == null)
                {
                    _trace_user_type = new UserTypeRepository(_repoContext);
                }
                return _trace_user_type;
            }
        }
        public IUWBAnchorMasterRepository IUWBAnchorMaster
        {
            get
            {
                if (_trace_uwb_anchormaster == null)
                {
                    _trace_uwb_anchormaster = new UWBAnchorMasterRepository(_repoContext);
                }
                return _trace_uwb_anchormaster;
            }
        }
        public IUWBTagMasterRepository IUWBTagMaster
        {
            get
            {
                if (_trace_uwbtag_master == null)
                {
                    _trace_uwbtag_master = new UWBTagMasterRepository(_repoContext);
                }
                return _trace_uwbtag_master;
            }
        }
        public IWorkCenterMasterRepository IWorkCenterMaster
        {
            get
            {
                if (_trace_workcenter_master == null)
                {
                    _trace_workcenter_master = new WorkCenterMasterRepository(_repoContext);
                }
                return _trace_workcenter_master;
            }
        }
        public IModuleMasterRespository IModuleMaster
        {
            get
            {
                if (_trace_module_master == null)
                {
                    _trace_module_master = new ModuleMasterRepository(_repoContext);
                }
                return _trace_module_master;
            }
        }
        public IApplicationEmployeeMappingRepository IApplicationEmployeeMapping
        {
            get
            {
                if (_trace_applicationemployee_mapping == null)
                {
                    _trace_applicationemployee_mapping = new ApplicationEmployeeMappingRepository(_repoContext);
                }
                return _trace_applicationemployee_mapping;
            }
        }
        public IApplicationMasterRepository IApplicationMaster
        {
            get
            {
                if (_trace_application_master == null)
                {
                    _trace_application_master = new ApplicationMasterRepository(_repoContext);
                }
                return _trace_application_master;
            }
        }
        public ILoginDetailsRepository ILoginDetails
        {
            get
            {
                if (_trace_login_details == null)
                {
                    _trace_login_details = new LoginDetailsRepository(_repoContext);
                }
                return _trace_login_details;
            }
        }
        public ILoginMasterRepository ILoginMaster
        {
            get
            {
                if (_trace_Login_master == null)
                {
                    _trace_Login_master = new LoginMasterRepository(_repoContext);
                }
                return _trace_Login_master;
            }
        }

        public async Task SaveAsync()
        {
            await _repoContext.SaveChangesAsync();
        }

    }
}
