﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Tread_FIFO;
using Entity.FGS_FIFO;
using Entity.TreadFIFO;
using Microsoft.EntityFrameworkCore;
namespace Repository.Tread_FIFO
{
    public class ProductionRemarksRepository : RepositoryBase<Trace_Production_Remarks>, IProductionRemarksRepository
    {
        public ProductionRemarksRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TraceProductionRemarksDTO>> GetTrace_Production_Remarks()
        {

            var rows = FindAll();
            var results = rows.Select(x => new TraceProductionRemarksDTO()
            {
                Remark_Id = x.Remark_Id,
                Remarks = x.Remarks,
                Remark_Status = x.Remark_Status,
                WorkCent_Id = x.WorkCent_Id,
              

            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<TraceProductionRemarksDTO>> GetTrace_Production_Remarks(int id)
        {


            var rows = FindByCondition(x => x.Remark_Id.Equals(id));
            var results = rows.Select(x => new TraceProductionRemarksDTO()
            {

                Remark_Id = x.Remark_Id,
                Remarks = x.Remarks,
                Remark_Status = x.Remark_Status,
                WorkCent_Id = x.WorkCent_Id,
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_Production_Remarks(TraceProductionRemarksDTO model)
        {
            var entity = new Trace_Production_Remarks();
            entity.Remark_Id = model.Remark_Id;
            entity.Remarks = model.Remarks;
            entity.Remark_Status = model.Remark_Status;
            entity.WorkCent_Id = model.WorkCent_Id;
          


            Create(entity);
        }
        public void UpdateTrace_Production_Remarks(TraceProductionRemarksDTO model)
        {
            var entity = new Trace_Production_Remarks();
            if (model != null)
            {
                if (model.Remark_Id > 0)
                {
                    var data = FindByCondition(x => x.Remark_Id == model.Remark_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                
                entity.Remark_Id = model.Remark_Id;
                entity.Remarks = model.Remarks;
                entity.Remark_Status = model.Remark_Status;
                entity.WorkCent_Id = model.WorkCent_Id;
            }
            Update(entity);
        }
        public void DeleteTrace_Production_Remarks(int id)
        {
            var entity = new Trace_Production_Remarks();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Remark_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                entity.Remark_Id = 0;
            }

            Update(entity);
        }
    }
}
