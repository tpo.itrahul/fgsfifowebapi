﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Tread_FIFO;
using Entity.TreadFIFO;
namespace Repository.Tread_FIFO
{
    public interface IProductAgeingRepository //: IRepositoryBase<Trace_Product_Ageing>
    {
        Task<IEnumerable<TraceProductAgeingDTO>> GetTrace_Product_Ageing();

        Task<IEnumerable<TraceProductAgeingDTO>> GetTrace_Product_Ageing(int id);

        void CreateTrace_Product_Ageing(TraceProductAgeingDTO model);

        void UpdateTrace_Product_Ageing(TraceProductAgeingDTO model);

        void DeleteTrace_Product_Ageing(int id);
        
    }
}
