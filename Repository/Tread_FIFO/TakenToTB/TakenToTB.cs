﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Tread_FIFO;
using Entity.FGS_FIFO;
using Entity.TreadFIFO;
using Microsoft.EntityFrameworkCore;
namespace Repository.Tread_FIFO
{
    public class TakenToTBRepository : RepositoryBase<Trace_Taken_ToTB>, ITakenToTBRepository
    {
        public TakenToTBRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {

        }
        public async Task<IEnumerable<TraceTakenToTbDTO>> GetTrace_Taken_ToTB()
        {

            var rows = FindAll();
            var results = rows.Select(x => new TraceTakenToTbDTO()
            {
                TakenToTb_Id = x.TakenToTb_Id,
                RequestID = x.RequestID,
                TakenToTB_ReqTime = x.TakenToTB_ReqTime,
                TakenToTb_IssuedTime = x.TakenToTb_IssuedTime,
                TakenToTB_By = x.TakenToTB_By,
                Mhe_Id = x.Mhe_Id,
                Production_Id = x.Production_Id,
                Status = x.Status,
                Issue_Status = x.Issue_Status

            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<TraceTakenToTbDTO>> GetTrace_Taken_ToTB(int id)
        {


            var rows = FindByCondition(x => x.TakenToTb_Id.Equals(id));
            var results = rows.Select(x => new TraceTakenToTbDTO()
            {
                TakenToTb_Id = x.TakenToTb_Id,
                RequestID = x.RequestID,
                TakenToTB_ReqTime = x.TakenToTB_ReqTime,
                TakenToTb_IssuedTime = x.TakenToTb_IssuedTime,
                TakenToTB_By = x.TakenToTB_By,
                Mhe_Id = x.Mhe_Id,
                Production_Id = x.Production_Id,
                Status = x.Status,
                Issue_Status = x.Issue_Status
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_Taken_ToTB(TraceTakenToTbDTO model)
        {
            var entity = new Trace_Taken_ToTB();
            entity.TakenToTb_Id = model.TakenToTb_Id;
            entity.RequestID = model.RequestID;
            entity.TakenToTB_ReqTime = model.TakenToTB_ReqTime;
            entity.TakenToTb_IssuedTime = model.TakenToTb_IssuedTime;
            entity.TakenToTB_By = model.TakenToTB_By;
            entity.TakenToTB_By = model.TakenToTB_By;
            entity.Mhe_Id = model.Mhe_Id;
            entity.Production_Id = model.Production_Id;
            entity.Status = model.Status;
            entity.Issue_Status = model.Issue_Status;
            Create(entity);
        }
        public void UpdateTrace_Taken_ToTB(TraceTakenToTbDTO model)
        {
            var entity = new Trace_Taken_ToTB();
            if (model != null)
            {
                if (model.TakenToTb_Id > 0)
                {
                    var data = FindByCondition(x => x.TakenToTb_Id == model.TakenToTb_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
              
                entity.TakenToTb_Id = model.TakenToTb_Id;
                entity.RequestID = model.RequestID;
                entity.TakenToTB_ReqTime = model.TakenToTB_ReqTime;
                entity.TakenToTb_IssuedTime = model.TakenToTb_IssuedTime;
                entity.TakenToTB_By = model.TakenToTB_By;
                entity.TakenToTB_By = model.TakenToTB_By;
                entity.Mhe_Id = model.Mhe_Id;
                entity.Production_Id = model.Production_Id;
                entity.Status = model.Status;
                entity.Issue_Status = model.Issue_Status;
            }
            Update(entity);
        }
        public void DeleteTrace_Taken_ToTB(int id)
        {
            var entity = new Trace_Taken_ToTB();
            if (id != 0)

            {
                var data = FindByCondition(x => x.TakenToTb_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                entity.Status = 0;
            }

            Update(entity);
        }
    }
}
