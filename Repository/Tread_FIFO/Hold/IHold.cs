﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Tread_FIFO;
using Entity.TreadFIFO;
namespace Repository.Tread_FIFO
{
    public interface IHoldRepository //: IRepositoryBase<Trace_Hold>
    {
        Task<IEnumerable<TraceHoldDTO>> GetTrace_Hold();

        Task<IEnumerable<TraceHoldDTO>> GetTrace_Hold(int id);

       void CreateTrace_Hold(TraceHoldDTO model);

        void UpdateTrace_Hold(TraceHoldDTO model);

        void DeleteTrace_Hold(int id);
       
    }
}
