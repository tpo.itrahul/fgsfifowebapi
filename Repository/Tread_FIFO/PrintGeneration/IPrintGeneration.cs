﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Tread_FIFO;
using Entity.TreadFIFO;
namespace Repository.Tread_FIFO
{
    public interface IPrintGenerationRepository // : IRepositoryBase<Trace_Print_Generation>
    {
        Task<IEnumerable<TracePrintGenerationDTO>> GetTrace_PrintGeneration();

        Task<IEnumerable<TracePrintGenerationDTO>> GetTrace_PrintGeneration(int id);

        void CreateTrace_PrintGeneration(TracePrintGenerationDTO model);

        void UpdateTrace_PrintGeneration(TracePrintGenerationDTO model);

        void DeleteTrace_PrintGeneration(int id);
        
    }
}
