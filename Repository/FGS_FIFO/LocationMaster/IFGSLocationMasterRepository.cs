﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;
using Entity.FGSFIFO;
namespace Repository.FGS_FIFO
{
    public interface IFGSLocationMasterRepository //: IRepositoryBase<FGS_FIFO_Location_Master>
    {
        Task<IEnumerable<LocationMasterDTO>> GetFGS_Location_Master();

        Task<IEnumerable<LocationMasterDTO>> GetFGS_Location_Master(int id);

        void CreateFGS_Location_Master(LocationMasterDTO model);

        void UpdateFGS_Location_Master(LocationMasterDTO model);

        void DeleteFGS_Location_Master(int id);
       
    }
}
