﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;
using Entity.FGSFIFO;
namespace Repository.FGS_FIFO
{
    public interface IFGSFGSDespatchRequestRepository
        //: IRepositoryBase<FGS_FIFO_Despatch_Request>
    {
        Task<IEnumerable<DailyDespatchRequestDTO> >GetFGS_Despatch_Request();
        void CreateFGS_Despatch_Request(DailyDespatchRequestDTO model);
        void UpdateFGS_Despatch_Request(DailyDespatchRequestDTO model);
        void DeleteFGS_Despatch_Request(int id);
        Task<IEnumerable<DailyDespatchRequestDTO>> GetFGS_Despatch_Request(int id);
         List<int> Depatch_RequestId_ForMaterial_ID(int Id);
         int TotQuantity_Despatch_Request_Placed_For_MID(int materialId);
        int TotQuantity_Despatch_Request_Placed_For_RID(int rid);

        List<MaterialMasterDTO> GetMaterial_ForCustomer_MaterialType(int custid, int materialtypeid);
        void ChangeTranscationStatus(int rid, int status);
    }

}
