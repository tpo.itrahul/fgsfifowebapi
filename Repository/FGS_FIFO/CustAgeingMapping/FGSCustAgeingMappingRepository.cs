﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;
using Entity.FGS_FIFO;
using Entity.FGSFIFO;
using Microsoft.EntityFrameworkCore;

namespace Repository.FGS_FIFO
{
    public class FGSCustAgeingMappingRepository : RepositoryBase<FGS_FIFO_Cust_Ageing_Mapping>, IFGSCustAgeingMappingRepository
    {
        public FGSCustAgeingMappingRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {
        }
        public async Task<IEnumerable<CustomerAgeingMappingDTO>> GetFGS_Cust_Ageing_Mapping()
        {

            var rows = FindAll().Include(x => x.FGS_FIFO_Customer_Master).Include(y =>y.FGS_FIFO_Ageing_Master);
            
            var results = rows.Select(x => new CustomerAgeingMappingDTO()
            {
                Cust_Ageing_Id = x.Cust_Ageing_Id,
                Customer_Id = x.Customer_Id,
                Ageing_Id = x.Ageing_Id,
                Ageing_Status = x.Ageing_Status,
                CustomerName = x.FGS_FIFO_Customer_Master.Customer_Name,
                AgeingMonths = Convert.ToInt32(x.FGS_FIFO_Ageing_Master.Ageing_Months),
                EntryDate = x.EntryDate


            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<CustomerAgeingMappingDTO>> GetFGS_Cust_Ageing_Mapping(int id)
        {


            var rows = FindByCondition(x => x.Cust_Ageing_Id.Equals(id));
            var results = rows.Select(x => new CustomerAgeingMappingDTO()
            {

                Cust_Ageing_Id = x.Cust_Ageing_Id,
                Customer_Id = x.Customer_Id,
                Ageing_Id = x.Ageing_Id,
                Ageing_Status = x.Ageing_Status,
              EntryDate = x.EntryDate


        }).ToListAsync();

            return await results;


        }
        public void CreateFGS_Cust_Ageing_Mapping(CustomerAgeingMappingDTO model)
        {
            var entity = new FGS_FIFO_Cust_Ageing_Mapping();
            entity.Cust_Ageing_Id = model.Cust_Ageing_Id;
            entity.Customer_Id = model.Customer_Id;
            entity.Ageing_Id = model.Ageing_Id;
            entity.Ageing_Status = model.Ageing_Status;
            entity.EntryDate = model.EntryDate;

            Create(entity);
        }
        public void UpdateFGS_Cust_Ageing_Mapping(CustomerAgeingMappingDTO model)
        {
            var entity = new FGS_FIFO_Cust_Ageing_Mapping();
            if (model != null)
            {
                if (model.Cust_Ageing_Id > 0)
                {
                    var data = FindByCondition(x => x.Cust_Ageing_Id == model.Cust_Ageing_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.Cust_Ageing_Id = model.Cust_Ageing_Id;
                entity.Customer_Id = model.Customer_Id;
                entity.Ageing_Id = model.Ageing_Id;
                entity.Ageing_Status =   model.Ageing_Status;
                entity.EntryDate = model.EntryDate;
            }
            Update(entity);
        }
        public void DeleteFGS_Cust_Ageing_Mapping(int id)
        {
            var entity = new FGS_FIFO_Cust_Ageing_Mapping();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Cust_Ageing_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                //updated entity status to 0 after finding the invoice id
                entity.Cust_Ageing_Id = 0;
            }

            Update(entity);
        }
    }
}
