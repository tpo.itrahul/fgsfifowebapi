﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.FGSFIFO;
namespace Repository.FGS_FIFO
{
    public interface IFGSAgeingMasterRepository 
        //: IRepositoryBase<FGS_FIFO_Ageing_Master>
    {
        Task<IEnumerable<AgeingMasterDTO>> GetFGS_Ageing();
        Task<IEnumerable<AgeingMasterDTO>> GetFGS_Ageing(int id);
        void CreateFGS_FGS_Ageing(AgeingMasterDTO model);

        void UpdateFGS_Ageing(AgeingMasterDTO model);

        void DeleteFGS_FGS_Ageing(int id);
                 
     }
}
