﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;
using Entity.FGS_FIFO;
using Entity.FGSFIFO;
using Microsoft.EntityFrameworkCore;

namespace Repository.FGS_FIFO
{
    public class FGSBatchTypeMasterRepository : RepositoryBase<FGS_FIFO_Batch_Type_Master>, IFGSBatchTypeMasterRepository
    {
        public FGSBatchTypeMasterRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {
        }
        public async Task<IEnumerable<BatchTypeMasterDTO>> GetFGS_Batch_Type_Master()
        {

            var rows = FindAll();
            var results = rows.Select(x => new BatchTypeMasterDTO()
            {
                Batch_Type_Id = x.Batch_Type_Id,
                Batch_Type_Name = x.Batch_Type_Name,
                Batch_Type_Status = x.Batch_Type_Status

            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<BatchTypeMasterDTO>> GetFGS_Batch_Type_Master(int id)
        {


            var rows = FindByCondition(x => x.Batch_Type_Id.Equals(id));
            var results = rows.Select(x => new BatchTypeMasterDTO()
            {

                Batch_Type_Id = x.Batch_Type_Id,
                Batch_Type_Name = x.Batch_Type_Name,
                Batch_Type_Status = x.Batch_Type_Status

            }).ToListAsync();

            return await results;


        }
        public void CreateFGS_Batch_Type_Master(BatchTypeMasterDTO model)
        {
            var entity = new FGS_FIFO_Batch_Type_Master();
            entity.Batch_Type_Id = model.Batch_Type_Id;
            entity.Batch_Type_Name = model.Batch_Type_Name;
            entity.Batch_Type_Status = model.Batch_Type_Status;
         

            Create(entity);
        }
        public void UpdateFGS_Batch_Type_Master(BatchTypeMasterDTO model)
        {
            var entity = new FGS_FIFO_Batch_Type_Master();
            if (model != null)
            {
                if (model.Batch_Type_Id > 0)
                {
                    var data = FindByCondition(x => x.Batch_Type_Id == model.Batch_Type_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.Batch_Type_Id = model.Batch_Type_Id;
                entity.Batch_Type_Name = model.Batch_Type_Name;
                entity.Batch_Type_Status =   model.Batch_Type_Status;
            }
            Update(entity);
        }
        public void DeleteFGS_Batch_Type_Master(int id)
        {
            var entity = new FGS_FIFO_Batch_Type_Master();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Batch_Type_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                entity.Batch_Type_Status = 0;
            }

            Update(entity);
        }

    }
}
