﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;
using Entity.FGS_FIFO;
using Entity.FGSFIFO;
using Microsoft.EntityFrameworkCore;
namespace Repository.FGS_FIFO
{
 
    public class FGSDailyDespatchTransactionRepository : RepositoryBase<FGS_FIFO_Daily_Despatch_Transaction>, IFGSDailyDespatchTransactionRepository
    {
        public FGSDailyDespatchTransactionRepository(Trace_DBContext repositoryContext) : base(repositoryContext)
        {
        }

        public async Task<IEnumerable<DailyDespatchTransactionDTO>> GetFGS_Daily_Despatch_Transaction()
        {

            var rows = FindAll()
                 .Include(sid => sid.FGS_FIFO_Despatch_Status_Master)
                 .Include(mid => mid.FGS_FIFO_Despatch_Request)

                 ;
            var results =  rows.Select(x => new DailyDespatchTransactionDTO()
            {
                Desp_TransId = x.Desp_TransId,
                Desp_Req_Id = x.Desp_Req_Id,
                Inv_TransId = x.Inv_TransId,
                Quantity_Despatched = x.Quantity_Despatched,
                EntryDate = x.EntryDate.ToShortDateString(),
                Desp_Status_Id = x.Desp_Status_Id,
                vehicle_number = x.vehicle_number,
                MaterialId = x.FGS_FIFO_Despatch_Request.Material_Id,


            }).ToListAsync();


            return await results;

        }
        public async Task<IEnumerable<DailyDespatchTransactionDTO>> GetFGS_Daily_Despatch_Transaction(int id)
        {
           var rows = FindByCondition(x => x.Desp_TransId.Equals(id))
                .Include(sid => sid.FGS_FIFO_Despatch_Status_Master)
                .Include(mid => mid.FGS_FIFO_Despatch_Request)
                ;
            var results = rows.Select(x => new DailyDespatchTransactionDTO()
            {
                Desp_TransId = x.Desp_TransId,
                Desp_Req_Id = x.Desp_Req_Id,
                Inv_TransId = x.Inv_TransId,
                Quantity_Despatched = x.Quantity_Despatched,
                EntryDate = x.EntryDate.ToShortDateString(),
                Desp_Status_Id = x.Desp_Status_Id,
                vehicle_number = x.vehicle_number,
                MaterialId = x.FGS_FIFO_Despatch_Request.Material_Id

            }).ToListAsync();


            return await results;

        }
        public int CreateFGS_Daily_Despatch_Transaction(DailyDespatchTransactionDTO model)
        {
            var entity = new FGS_FIFO_Daily_Despatch_Transaction();
           

            if (model != null)
            {
              
                entity.Desp_TransId = model.Desp_TransId;
                entity.Desp_Req_Id = model.Desp_Req_Id;
                entity.Inv_TransId = model.Inv_TransId;
                entity.Quantity_Despatched = model.Quantity_Despatched;
                entity.EntryDate =Convert.ToDateTime(model.EntryDate);
               entity.Desp_Status_Id = 1;
                entity.vehicle_number = model.vehicle_number;
                entity.Barcode = model.Barcode;
              
             }

            Create(entity);
            return entity.Desp_TransId;
            //update despatch status in daily despatch transaction  after succesfully placing the scanned depatch.
   

        }
        public void UpdateFGS_Daily_Despatch_Transaction(DailyDespatchTransactionDTO model)
        {
            var entity = new FGS_FIFO_Daily_Despatch_Transaction();

            if (model != null)
            {
                if (model.Desp_TransId > 0)
                {
                    var data = FindByCondition(x => x.Desp_TransId == model.Desp_TransId);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                int despatchedquantity = Get_tot_despatchedForRequestId(model.Desp_Req_Id);
               

                if (model.Desp_TransId>0) { 
                entity.Desp_TransId = model.Desp_TransId;
                }
                if(model.Desp_Req_Id!=0)
                { 
                entity.Desp_Req_Id = model.Desp_Req_Id;
                }
                if (model.Inv_TransId!=0) { 
                entity.Inv_TransId = model.Inv_TransId;
                }
                if ( model.Quantity_Despatched != 0) { 
                entity.Quantity_Despatched = model.Quantity_Despatched;
                }
                if (Convert.ToDateTime(model.EntryDate) != DateTime.MinValue && Convert.ToDateTime(model.EntryDate) != DateTime.MaxValue) { entity.EntryDate = Convert.ToDateTime(model.EntryDate); }
              
                if (model.Desp_Status_Id !=0) { 
                entity.Desp_Status_Id = model.Desp_Status_Id;
                }
                if (!string.IsNullOrEmpty(model.vehicle_number) )
                    { 
                entity.vehicle_number = model.vehicle_number;
                }

            }
            Update(entity);
        }
        public void DeleteFGS_Daily_Despatch_Transaction(int id)
        {

            var entity = new FGS_FIFO_Daily_Despatch_Transaction();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Desp_TransId == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                //updated entity status to 0 after finding the invoice id
                entity.Desp_Status_Id = 0;
            }

            Update(entity);
        }
        public int Get_tot_despatchedForRequestId(int id)
        {
            //var entity = new FGS_FIFO_Daily_Despatch_Transaction();
            //var Model = new DailyDespatchTransactionDTO();
            int tot = 0;
            if (id != 0)
             
            {
               
                var data = FindByCondition(x => x.Desp_Req_Id == id);
                if (data.Any())
                {
                   
                    foreach (var qnty in data)
                    {
                        tot = tot + qnty.Quantity_Despatched;
                        
                    }
                
                    
                }
                return tot;
            }
            return tot;


        }
        public int Get_tot_DespatchedForMaterialId(int id)
        {
            //var entity = new FGS_FIFO_Daily_Despatch_Transaction();
            //var Model = new DailyDespatchTransactionDTO();
            int tot = 0;
            if (id != 0)

            {

                var data = FindByCondition(x => x.Desp_Req_Id == id);
                if (data.Any())
                {

                    foreach (var qnty in data)
                    {
                        tot = tot + qnty.Quantity_Despatched;

                    }


                }
                return tot;
            }
            return tot;


        }

        public int Get_tot_DespatchedForScannedBarcode(string id)
        {

            int tot = 0;
            if (id != null)

            {

                var data = FindByCondition(x => x.Barcode == id);
                if (data.Any())
                {

                    foreach (var qnty in data)
                    {
                        tot = tot + qnty.Quantity_Despatched;

                    }


                }
                return tot;
            }
            return tot;


        }
        public void ChangeTranscationProcessStatus(int tid, int status)
        {
            var entity = new FGS_FIFO_Daily_Despatch_Transaction();
            var data = FindByCondition(x => x.Desp_Req_Id == tid);
            if (data.Any())
            {
                entity = data.FirstOrDefault();
                entity.Desp_Status_Id = status;

            }
            Update(entity);
        }




    }
}
