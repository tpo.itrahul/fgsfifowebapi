﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Trace_Masters;
using Entity.TraceMasters;
namespace Repository.Trace_Masters
{
    public interface IUWBAnchorMasterRepository //: IRepositoryBase<Trace_UWB_Anchor_Master>
    {
        Task<IEnumerable<TraceUWBAnchorMasterDTO>> GetTrace_UWB_Anchor_Master();

        Task<IEnumerable<TraceUWBAnchorMasterDTO>> GetTrace_UWB_Anchor_Master(int id);

        void CreateTrace_UWB_Anchor_Master(TraceUWBAnchorMasterDTO model);

        void UpdateTrace_UWB_Anchor_Master(TraceUWBAnchorMasterDTO model);

        void DeleteTrace_UWB_Anchor_Master(int id);
       
    }
}
