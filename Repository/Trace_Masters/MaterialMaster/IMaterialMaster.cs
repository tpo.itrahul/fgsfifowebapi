﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Trace_Masters;
using Entity.TraceMasters;
namespace Repository.Trace_Masters
{
    public interface IMaterialMasterRepository 
        //: IRepositoryBase<Trace_Material_Master>
    {
        Task<IEnumerable<TraceMaterialMasterDTO>> GetFGS__Material_Master();

        Task<IEnumerable<TraceMaterialMasterDTO>> GetFGS__Material_Master(int id);

       void CreateFGS__Material_Master(TraceMaterialMasterDTO model);

       void UpdateFGS__Material_Master(TraceMaterialMasterDTO model);

        void DeleteFGS__Material_Master(int id);
       
    }
}
