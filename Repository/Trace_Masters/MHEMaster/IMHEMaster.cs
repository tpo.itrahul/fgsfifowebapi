﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Trace_Masters;
using Entity.TraceMasters;
namespace Repository.Trace_Masters
{
    public interface IMHEMasterRepository : IRepositoryBase<Trace_MHE_Master>
    {
        Task<IEnumerable<TraceMHEMasterDTO>> GetTrace_MHE_Master();

        Task<IEnumerable<TraceMHEMasterDTO>> GetTrace_MHE_Master(int id);

        void CreateTrace_MHE_Master(TraceMHEMasterDTO model);

        void UpdateTrace_MHE_Master(TraceMHEMasterDTO model);

        void DeleteTrace_MHE_Master(int id);
       
    }
}
