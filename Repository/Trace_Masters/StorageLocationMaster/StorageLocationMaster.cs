﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Trace_Masters;
using Entity.FGS_FIFO;
using Entity.TraceMasters;
using Microsoft.EntityFrameworkCore;
namespace Repository.Trace_Masters
{
    public class StorageLocationMasterRepository : RepositoryBase<Trace_Storage_Location_Master>, IStorageLocationMasterRepository
    {
        public StorageLocationMasterRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TraceStorageLocationMasterDTO>> GetTrace_Storage_Location_Master()
        {

            var rows = FindAll();
            var results = rows.Select(x => new TraceStorageLocationMasterDTO()
            {
              
                Storage_Loc_Id = x.Storage_Loc_Id,
                Storage_Loc_Name = x.Storage_Loc_Name,
                Storage_Loc_Status = x.Storage_Loc_Status,
                WorkCent_Id = x.WorkCent_Id,
                Type = x.Type

            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<TraceStorageLocationMasterDTO>> GetTrace_Storage_Location_Master(int id)
        {


            var rows = FindByCondition(x => x.Storage_Loc_Id.Equals(id));
            var results = rows.Select(x => new TraceStorageLocationMasterDTO()
            {
                Storage_Loc_Id = x.Storage_Loc_Id,
                Storage_Loc_Name = x.Storage_Loc_Name,
                Storage_Loc_Status = x.Storage_Loc_Status,
                WorkCent_Id = x.WorkCent_Id,
                Type = x.Type
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_Storage_Location_Master(TraceStorageLocationMasterDTO model)
        {
            var entity = new Trace_Storage_Location_Master();

           
            entity.Storage_Loc_Id = model.Storage_Loc_Id;
            entity.Storage_Loc_Name = model.Storage_Loc_Name;
            entity.Storage_Loc_Status = model.Storage_Loc_Status;
            entity.WorkCent_Id = model.WorkCent_Id;
            entity.Type = model.Type;

            Create(entity);
        }
        public void UpdateTrace_Storage_Location_Master(TraceStorageLocationMasterDTO model)
        {
            var entity = new Trace_Storage_Location_Master();
            if (model != null)
            {
                if (model.Storage_Loc_Id > 0)
                {
                    var data = FindByCondition(x => x.Storage_Loc_Id == model.Storage_Loc_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }

                entity.Storage_Loc_Id = model.Storage_Loc_Id;
                entity.Storage_Loc_Name = model.Storage_Loc_Name;
                entity.Storage_Loc_Status = model.Storage_Loc_Status;
                entity.WorkCent_Id = model.WorkCent_Id;
                entity.Type = model.Type;

                Create(entity);


            }
            Update(entity);
        }
        public void DeleteTrace_Storage_Location_Master(int id)
        {
            var entity = new Trace_Storage_Location_Master();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Storage_Loc_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                entity.Storage_Loc_Status = 0;
            }

            Update(entity);
        }
    }
}
