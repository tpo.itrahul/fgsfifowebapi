﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Trace_Masters;
using Entity.TraceMasters;
namespace Repository.Trace_Masters
{
    public interface IStorageLocationMasterRepository //: IRepositoryBase<Trace_Storage_Location_Master>
    {
       Task<IEnumerable<TraceStorageLocationMasterDTO>> GetTrace_Storage_Location_Master();

       Task<IEnumerable<TraceStorageLocationMasterDTO>> GetTrace_Storage_Location_Master(int id);

       void CreateTrace_Storage_Location_Master(TraceStorageLocationMasterDTO model);

         void UpdateTrace_Storage_Location_Master(TraceStorageLocationMasterDTO model);

        void DeleteTrace_Storage_Location_Master(int id);
      

    }
}
