﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Trace_Masters;
using Entity.TraceMasters;
namespace Repository.Trace_Masters
{
    public interface IUserTypeRepository 
        //: IRepositoryBase<Trace_User_Type>
    {
        Task<IEnumerable<TraceUserTypeDTO>> GetTrace_User_Type();

        Task<IEnumerable<TraceUserTypeDTO>> GetTrace_User_Type(int id);

        void CreateTrace_User_Type(TraceUserTypeDTO model);

        void UpdateTrace_User_Type(TraceUserTypeDTO model);

        void DeleteTrace_User_Type(int id);
       

    }
}
