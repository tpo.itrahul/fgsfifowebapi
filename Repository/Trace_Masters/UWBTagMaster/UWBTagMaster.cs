﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Trace_Masters;
using Entity.FGS_FIFO;
using Entity.TraceMasters;
using Microsoft.EntityFrameworkCore;
namespace Repository.Trace_Masters
{
    public class UWBTagMasterRepository : RepositoryBase<Trace_UWB_Tag_Master>, IUWBTagMasterRepository
    {
        public UWBTagMasterRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {

        }
        public async Task<IEnumerable<TraceUWBTagMasterDTO>> GetTrace_UWB_Tag_Master()
        {

            var rows = FindAll();
            var results = rows.Select(x => new TraceUWBTagMasterDTO()
            {
                UWB_Tag_Id = x.UWB_Tag_Id,
                UWB_Tag_Name = x.UWB_Tag_Name,
                UWB_Tag_Uniqe_Id = x.UWB_Tag_Uniqe_Id,
                UWB_Tag_Status = x.UWB_Tag_Status,

            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<TraceUWBTagMasterDTO>> GetTrace_UWB_Tag_Master(int id)
        {


            var rows = FindByCondition(x => x.UWB_Tag_Id.Equals(id));
            var results = rows.Select(x => new TraceUWBTagMasterDTO()
            {
                UWB_Tag_Id = x.UWB_Tag_Id,
                UWB_Tag_Name = x.UWB_Tag_Name,
                UWB_Tag_Uniqe_Id = x.UWB_Tag_Uniqe_Id,
                UWB_Tag_Status = x.UWB_Tag_Status,
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_UWB_Tag_Master(TraceUWBTagMasterDTO model)
        {
            var entity = new Trace_UWB_Tag_Master();
            entity.UWB_Tag_Id = model.UWB_Tag_Id;
            entity.UWB_Tag_Name = model.UWB_Tag_Name;
            entity.UWB_Tag_Uniqe_Id = model.UWB_Tag_Uniqe_Id;
            entity.UWB_Tag_Status = model.UWB_Tag_Status;

            Create(entity);
        }
        public void UpdateTrace_UWB_Tag_Master(TraceUWBTagMasterDTO model)
        {
            var entity = new Trace_UWB_Tag_Master();
            if (model != null)
            {
                if (model.UWB_Tag_Id > 0)
                {
                    var data = FindByCondition(x => x.UWB_Tag_Id == model.UWB_Tag_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.UWB_Tag_Id = model.UWB_Tag_Id;
                entity.UWB_Tag_Name = model.UWB_Tag_Name;
                entity.UWB_Tag_Uniqe_Id = model.UWB_Tag_Uniqe_Id;
                entity.UWB_Tag_Status = model.UWB_Tag_Status;

            }
            Update(entity);
        }
        public void DeleteTrace_UWB_Tag_Master(int id)
        {
            var entity = new Trace_UWB_Tag_Master();
            if (id != 0)

            {
                var data = FindByCondition(x => x.UWB_Tag_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                entity.UWB_Tag_Id = 0;
            }

            Update(entity);
        }
    }
}
