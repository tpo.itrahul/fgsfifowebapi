﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Trace_Masters;
using Entity.TraceMasters;
namespace Repository.Trace_Masters
{
    public interface IUWBTagMasterRepository// : IRepositoryBase<Trace_UWB_Tag_Master>
    {
        Task<IEnumerable<TraceUWBTagMasterDTO>> GetTrace_UWB_Tag_Master();

        Task<IEnumerable<TraceUWBTagMasterDTO>> GetTrace_UWB_Tag_Master(int id);

        void CreateTrace_UWB_Tag_Master(TraceUWBTagMasterDTO model);

        void UpdateTrace_UWB_Tag_Master(TraceUWBTagMasterDTO model);

        void DeleteTrace_UWB_Tag_Master(int id);
        
    }
}
