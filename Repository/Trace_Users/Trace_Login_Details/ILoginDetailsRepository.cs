﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.TraceUsers;
using Entity.TraceUsers;
namespace Repository.Trace_Users
{
    public interface ILoginDetailsRepository //: IRepositoryBase<Trace_Barcode_Generation>
    {
         Task<IEnumerable<Trace_Login_DetailsDTO>> GetTrace_Login_Details();

        Task<IEnumerable<Trace_Login_DetailsDTO>> GetTrace_Login_Details(int id);

       void CreateTrace_Login_Details(Trace_Login_DetailsDTO model);

       void UpdateTrace_Login_Details(Trace_Login_DetailsDTO model);

       void DeleteTrace_Login_Details(int id);
       


    }
}
