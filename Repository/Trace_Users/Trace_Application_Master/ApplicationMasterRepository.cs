﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.TraceUsers;
using Entity.FGS_FIFO;
using Entity.TraceUsers;
using Microsoft.EntityFrameworkCore;
namespace Repository.Trace_Users
{
    public class ApplicationMasterRepository : RepositoryBase<Trace_Application_Master>, IApplicationMasterRepository
    {
        public ApplicationMasterRepository(Trace_DBContext repositoryContext) : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<Trace_Application_MasterDTO>> GetTrace_Application_Master()
        {

            var rows = FindAll();
            var results = rows.Select(x => new Trace_Application_MasterDTO()
            {
                Id = x.Id,
                Application_Id = x.Application_Id,
                Application_Name = x.Application_Name,
                Application_Status = x.Application_Status,
                Plant_Id = x.Plant_Id
            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<Trace_Application_MasterDTO>> GetTrace_Application_Master(int id)
        {


            var rows = FindByCondition(x => x.Id.Equals(id));
            var results = rows.Select(x => new Trace_Application_MasterDTO()
            {
                Id = x.Id,
                Application_Id = x.Application_Id,
                Application_Name = x.Application_Name,
                Application_Status = x.Application_Status,
                Plant_Id = x.Plant_Id
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_Application_Master(Trace_Application_MasterDTO model)
        {
            var entity = new Trace_Application_Master();
            entity.Id = model.Id;
            entity.Application_Id = model.Application_Id;
            entity.Application_Name = model.Application_Name;
            entity.Application_Status = model.Application_Status;
            entity.Plant_Id = model.Plant_Id;
         

            Create(entity);
        }
        public void UpdateTrace_Application_Master(Trace_Application_MasterDTO model)
        {
            var entity = new Trace_Application_Master();
            if (model != null)
            {
                if (model.Id > 0)
                {
                    var data = FindByCondition(x => x.Id== model.Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.Id = model.Id;
                entity.Application_Id = model.Application_Id;
                entity.Application_Name = model.Application_Name;
                entity.Application_Status = model.Application_Status;
                entity.Plant_Id = model.Plant_Id;
            }
            Update(entity);
        }
        public void DeleteTrace_Application_Master(int id)
        {
            var entity = new Trace_Application_Master();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                entity.Id = 0;
            }

            Update(entity);
        }
    }
}
