﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.TraceUsers;
using Entity.TraceUsers;
namespace Repository.Trace_Users
{
    public interface IApplicationMasterRepository //: IRepositoryBase<Trace_Barcode_Generation>
    {
         Task<IEnumerable<Trace_Application_MasterDTO>> GetTrace_Application_Master();

        Task<IEnumerable<Trace_Application_MasterDTO>> GetTrace_Application_Master(int id);

       void CreateTrace_Application_Master(Trace_Application_MasterDTO model);

       void UpdateTrace_Application_Master(Trace_Application_MasterDTO model);

       void DeleteTrace_Application_Master(int id);
       


    }
}
