﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.TraceUsers;
using Entity.FGS_FIFO;
using Entity.TraceUsers;
using Microsoft.EntityFrameworkCore;
namespace Repository.Trace_Users
{
    public class ApplicationEmployeeMappingRepository : RepositoryBase<Trace_Application_Employee_Mapping>, IApplicationEmployeeMappingRepository
    {
        public ApplicationEmployeeMappingRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {
        }
        public async Task<IEnumerable<Trace_Application_Employee_MappingDTO>> GetTrace_Application_Employee_Mapping()
        {

            var rows = FindAll();
            var results = rows.Select(x => new Trace_Application_Employee_MappingDTO()
            {
                Id = x.Id,
                LogMastEmp_Id = x.LogMastEmp_Id,
                Module_Id = x.Module_Id,
                Delete = x.Delete,
                Update = x.Update,
                View = x.View ,
               Add = x.Add,
               
            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<Trace_Application_Employee_MappingDTO>> GetTrace_Application_Employee_Mapping(int id)
        {


            var rows = FindByCondition(x => x.LogMastEmp_Id.Equals(id));
            var results = rows.Select(x => new Trace_Application_Employee_MappingDTO()
            {
                Id = x.Id,
                LogMastEmp_Id = x.LogMastEmp_Id,
                Module_Id = x.Module_Id,
                Delete = x.Delete,
                Update = x.Update,
                View = x.View,
               Add = x.Add,
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_Application_Employee_Mapping(Trace_Application_Employee_MappingDTO model)
        {
            var entity = new Trace_Application_Employee_Mapping();

            entity.Id = model.Id;
            entity.LogMastEmp_Id = model.LogMastEmp_Id;
            entity.Module_Id = model.Module_Id;
            entity.Delete = model.Delete;
            entity.Update = model.Update;
            entity.View = model.View;
            entity.Add = model.Add;


            Create(entity);
        }
        public void UpdateTrace_Application_Employee_Mapping(Trace_Application_Employee_MappingDTO model)
        {
            var entity = new Trace_Application_Employee_Mapping();
            if (model != null)
            {
                if (model.Id > 0)
                {
                    var data = FindByCondition(x => x.Id == model.Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.Id = model.Id;
                entity.LogMastEmp_Id = model.LogMastEmp_Id;
                entity.Module_Id = model.Module_Id;
                entity.Delete = model.Delete;
                entity.Update = model.Update;
                entity.View = model.View;
                entity.Add = model.Add;
            }
            Update(entity);
        }
        public void DeleteTrace_Application_Employee_Mapping(int id)
        {
            var entity = new Trace_Application_Employee_Mapping();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                entity.Id = 0;
            }

            Update(entity);
        }
    }
}
